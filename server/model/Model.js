const db = require('../database').database;
const QueryGenerator = require('../database').QueryGenerator;
const Validator = require('../validator').Validator;
const Errors = require('../Errors');
const uuid = require('node-uuid');

/**
 * モデル
 * @class Model
 */
class Model {
  /**
   * @param {Object} data データ
   * @param {boolean} [isFromDb=false] データベースから読み込まれたか
   */
  constructor(data, isFromDb) {
    this.isFromDb = !!isFromDb;

    data.id = (this.isFromDb) ? data.id : this.getNextId();
    this.data = data;
  }

  /**
   * スキーマを設定する
   * @static
   * @param {Object} schema スキーマ
   * @param {string} schema.name スキーマ名（=テーブル名）
   * @param {Object} fields フィールド定義（key:フィールド名、value: Joiオブジェクト）
   */
  static setSchema(schema) {
    const name = schema.name;
    const fields = Object.keys(schema.fields);

    this.prototype.name = name;
    this.prototype.columns = fields;
    this.prototype.query = new QueryGenerator(name, fields);
    this.prototype.validator = new Validator(schema.fields);
  }

  /**
   * モデルを検索する
   * @static
   * @param {Object} params パラメータ
   * @param {Array.<string>} [params.columns] 取得するカラム
   * @param {Object} [params.where] 検索条件
   * @returns {Promise.<Array.<Model>>} モデル
   */
  static find(params) {
    params = params || {};
    const query = this.query.select(params);

    return db.query(query.text, query.values)
      .then((records) => records.map(record => new this(record, true)));
  }

  /**
   * 指定のIDに紐づくモデルを検索する
   * @static
   * @param {string} id ID
   * @returns {Promise.<Model|Error>} モデル
   */
  static findById(id) {
    return this.find({
      where: { id: id }
    })
      .then((models) => {
        const model = models.shift();
        if (!model) throw Errors.badRequest(`ID:${id}のデータは存在しません`);
        return model;
      });
  }

  /**
   * モデルを新規作成する
   * @static
   * @param {Object} data データ
   * @returns {Promise.<Model>} 作成されたモデル
   */
  static create(data) {
    const model = new this(data);
    return model.save();
  }

  /**
   * 指定のIDに紐づくモデルを更新する
   * @static
   * @param {string} id ID
   * @param {Object} data データ
   * @returns {Promise.<Model>} 更新されたモデル
   */
  static update(id, data) {
    return this.findById(id)
      .then((model) => {
        model.read(data);
        return model.save();
      });
  }

  /**
   * 指定のIDに紐づくモデルを削除する
   * @static
   * @param {string} id ID
   * @returns {Promise.<Model>} 削除されたモデル
   */
  static destroy(id) {
    return this.findById(id)
      .then(model => model.destroy());
  }

  /**
   * スキーマ名
   * @readonly
   * @static
   * @type {string}
   */
  static get schemaName() {
    return this.prototype.name;
  }

  /**
   * フィールド名
   * @readonly
   * @static
   * @type {Array.<string>}
   */
  static get fields() {
    return this.prototype.columns;
  }

  /**
   * クエリ生成
   * @readonly
   * @static
   * @type {QueryGenerator}
   */
  static get query() {
    return this.prototype.query;
  }

  /**
   * ID
   * @readonly
   * @type {string}
   */
  get id() {
    return this.data.id;
  }

  /**
   * フィールドの値を取得する
   * @param {string} field フィールド名
   * @returns {*} 値
   */
  get(field) {
    return this.data[field];
  }

  /**
   * フィールドに値を設定する
   * @param {string} field フィールド名
   * @param {*} value 値
   */
  set(field, value) {
    if (!this.hasField(field)) {
      throw Errors.badRequest(`フィールド:${field}はモデル:${this.name}に定義されていません`);
    }
    this.data[field] = value;
  }

  /**
   * データを設定する
   * @param {Object} data データ
   */
  read(data) {
    const keys = Object.keys(data);

    keys.forEach((key) => {
      this.set(key, data[key]);
    });
  }

  /**
   * データが適切か確認する
   */
  validate() {
    const result = this.validator.validate(this.data);

    if (!result.isValid) {
      throw Errors.badRequest('フィールドの値が不正です', result.errors);
    } else {
      this.data = result.data;
    }
  }

  /**
   * 新規レコードであるか判定する
   * @returns {boolean} true: 新規レコード、false: 登録済みレコード
   */
  isNew() {
    return !this.isFromDb;
  }

  /**
   * 指定のフィールドを保持しているか判定する
   * @param {string} field フィールド名
   * @returns {boolean} true: 保持している、false: 保持していない
   */
  hasField(field) {
    return (this.columns.indexOf(field) >= 0);
  }

  /**
   * 保存する
   * @returns {Promise.<Model|Error>} <code>this</code>
   */
  save() {
    this.validate();
    this._beforeSave();
    return this._executeSave();
  }

  /**
   * 保存前の処理を行う
   */
  _beforeSave() {
    if (this.isNew()) {
      this.setCreatedAt();
    }
    this.setUpdatedAt();
  }

  /**
   * 保存処理を行う
   * @returns {Promise.<Model|Error>} <code>this</code>
   */
  _executeSave() {
    const query = (() => {
      if (this.isNew()) {
        return this.query.insert({
          data: this.data
        });
      } else {
        return this.query.update({
          data: this.data,
          where: { id: this.id }
        });
      }
    })();

    return db.query(query.text, query.values)
      .then(() => this);
  }

  /**
   * 破棄する
   * @returns {Promise.<Model|Error>} <code>this</code>
   */
  destroy() {
    const query = this.query.delete({
      where: { id: this.id }
    });
    return db.query(query.text, query.values)
      .then(() => this);
  }

  /**
   * IDを採番する
   * @returns {string} ID
   */
  getNextId() {
    return uuid.v4();
  }

  /**
   * 作成日時を現在の日時で更新する
   */
  setCreatedAt() {
    if (!this.hasField('created_at')) return;
    this.set('created_at', new Date());
  }

  /**
   * 更新日時を現在の日時で更新する
   */
  setUpdatedAt() {
    if (!this.hasField('updated_at')) return;
    this.set('updated_at', new Date());
  }

  /**
   * JSON形式に変換する
   * @returns {Object}
   */
  toJSON() {
    return this.data;
  }
}

module.exports = Model;
