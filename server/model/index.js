const ModelManager = require('./ModelManager');
const modelManager = new ModelManager();

const MasterCodes = require('./MasterCodes');
const Samples = require('./Samples');

modelManager.load([
  MasterCodes,
  Samples
]);

module.exports = modelManager;
