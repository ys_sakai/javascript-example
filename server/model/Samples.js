const Model = require('./Model');
const Joi = require('../validator').Joi;

/**
 * サンプル
 * @class Samples
 * @extends {Model}
 */
class Samples extends Model {}

Samples.setSchema({
  name: 'samples',
  fields: {
    id: Joi.string().guid().required(),
    first_name: Joi.string().max(20).required(),
    last_name: Joi.string().max(20).required(),
    sex: Joi.string().length(2),
    job_category: Joi.string().length(2),
    certificate: Joi.number().integer(),
    remarks: Joi.string().max(400).allow(''),
    created_at: Joi.date().timestamp(),
    updated_at: Joi.date().timestamp()
  }
});

module.exports = Samples;
