/**
 * モデル管理
 * @class ModelManager
 */
class ModelManager {
  constructor() {
    this.Models = {};
  }

  /**
   * モデルクラスを読み込む
   * @param {Array.<Function>} Models モデルクラスの配列
   */
  load(Models) {
    Models.forEach((Model) => {
      this.add(Model.schemaName, Model);
    });
  }

  /**
   * モデルクラスを登録する
   * @param {string} name モデル名
   * @param {Function} Model モデルクラス
   */
  add(name, Model) {
    this.Models[name] = Model;
  }

  /**
   * モデルクラスを削除する
   * @param {string} name モデル名
   */
  remove(name) {
    delete this.Models[name];
  }

  /**
   * モデルクラスを取得する
   * @param {string} name モデル名
   * @returns {Function} モデルクラス
   */
  get(name) {
    const Model = this.Models[name];
    if (!Model) return null;
    return Model;
  }
}

module.exports = ModelManager;
