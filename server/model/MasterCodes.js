const Model = require('./Model');
const Joi = require('../validator').Joi;

/**
 * コードマスタ
 * @class Samples
 * @extends {Model}
 */
class MasterCodes extends Model {}

MasterCodes.setSchema({
  name: 'master_codes',
  fields: {
    id: Joi.string().guid().required(),
    group_code: Joi.string().length(3).required(),
    code: Joi.string().length(3).required(),
    group_name: Joi.string().max(20),
    name: Joi.string().max(40)
  }
});

module.exports = MasterCodes;
