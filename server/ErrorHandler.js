const ErrorResponse = require('./Response');

const env = process.env.NODE_ENV || 'development';

/**
 * エラーハンドラ
 * @class ErrorHandler
 */
class ErrorHandler {
  /**
   * エラーハンドラをExpressのエラー用ミドルウェアに設定する
   * @static
   * @returns {function} Expressのエラー用ミドルウェア
   */
  static expressApp() {
    return (error, request, response, next) => {
      this.handleError(error, request)
        .then((handler) => {
          if (response.headersSent) {
            next(error);
          } else {
            response.status(handler.status);
            response.json(handler.response);
          }
        });
    };
  }

  /**
   * エラー処理のハンドラ
   * @static
   * @param {Error} error エラー
   * @param {Object} request リクエスト
   * @returns {Promise.<ErrorHandler>} エラーハンドラ
   */
  static handleError(error, request) {
    const handler = new this(error, request);
    return Promise.resolve(handler.handle());
  }

  /**
   * @param {Error} error
   * @param {Object} request
   */
  constructor(error, request) {
    this.error = error;
    this.request = request;

    this.data = null;
    this.message = null;
  }

  /**
   * ステータスコード
   * @readonly
   * @type {number}
   */
  get status() {
    if (!Number.isInteger(this.error.status)) return 500;
    return this.error.status;
  }

  /**
   * レスポンス
   * @readonly
   * @type {Response}
   */
  get response() {
    return new ErrorResponse({
      status: 'error',
      data: this.data,
      message: this.message
    });
  }

  /**
   * エラーを処理する
   * @returns {ErrorHandler} <code>this</code>
   */
  handle() {
    this.log();

    this.data = this.getResponseData();
    this.message = this.getMessage();

    return this;
  }

  /**
   * エラーを記録する
   */
  log() {
    if (this.is4xxError()) {
      console.warn(this.error.message, this.error.data);
    } else {
      console.error(this.error.stack);
    }
  }

  /**
   * レスポンス用のデータを取得する
   * @returns {Object|null} データ
   */
  getResponseData() {
    if (this.error.data) return this.error.data;

    if (env === 'development') {
      return this.error.stack.split(/\r\n|\r|\n/);
    } else {
      return null;
    }
  }

  /**
   * レスポンス用のメッセージを取得する
   * @returns {string} メッセージ
   */
  getMessage() {
    if (this.is4xxError() || env === 'development') {
      return this.error.message;
    } else {
      return 'サーバ内でエラーが発生しました';
    }
  }

  /**
   * クライアントエラー（4xx）か判定する
   * @returns {boolean}
   */
  is4xxError() {
    return this.status >= 400 && this.status < 500;
  }
}

module.exports = ErrorHandler;
