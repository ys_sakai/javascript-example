/**
 * joiのエラーメッセージ（日本語対応）
 * joi/lib/language.js
 * https://github.com/hapijs/joi/blob/v8.0.5/lib/language.js
 * の各項目をオーバーライドすることで、エラーメッセージを変更する
 * @type {Object}
 */
const language = {
  root: 'value',
  key: '"{{!key}}" ',
  messages: {
    wrapArrays: true
  },
  any: {
    unknown: '!!許可されていない値です',
    invalid: '!!無効な値が含まれています',
    empty: '!!空の値は許可されていません',
    required: '!!必須な値です',
    allowOnly: '!!{{valids}}のうちいずれかにしてください',
    default: '!!default method実行中に例外が発生しました'
  },
  alternatives: {
    base: '!!許可されたいずれのalternativeにも一致しません'
  },
  array: {
    base: '!!配列型にしてください',
    includes: '!!インデックス{{pos}}が許可されたいずれの型にも一致しません',
    includesSingle: '!!"{{!key}}"の値が許可されたいずれの型にも一致しません',
    includesOne: '!!インデックス{{pos}}が次の理由により失敗しました：{{reason}}',
    includesOneSingle: '!!"{{!key}}"の値が次の理由により失敗しました：{{reason}}',
    includesRequiredUnknowns: '!!必須な値{{unknownMisses}}項目が含まれていません',
    includesRequiredKnowns: '!!{{knownMisses}}が含まれていません',
    includesRequiredBoth: '!!{{knownMisses}}と他の必須な値{{unknownMisses}}項目が含まれていません',
    excludes: '!!インデックス{{pos}}に許可されていない値が含まれています',
    excludesSingle: '!!"{{!key}}"の値に許可されていない値が含まれています',
    min: '!!要素数を{{limit}}以上にしてください',
    max: '!!要素数を{{limit}}以下にしてください',
    length: '!!要素数を{{limit}}にしてください',
    ordered: '!!インデックス{{pos}}が次の理由により失敗しました：{{reason}}',
    orderedLength: '!!インデックス{{pos}}が次の理由により失敗しました：要素数を{{limit}}以下にしてください',
    sparse: '!!疎な配列にしないでください',
    unique: '!!インデックス{{pos}}に重複した値が含まれています'
  },
  boolean: {
    base: '!!ブーリアン型にしてください'
  },
  binary: {
    base: '!!Buffer型または文字列型にしてください',
    min: '!!{{limit}}バイト以上にしてください',
    max: '!!{{limit}}バイト以下にしてください',
    length: '!!{{limit}}バイトにしてください'
  },
  date: {
    base: '!!ミリ秒単位の数値または有効な日付文字列にしてください',
    min: '!!"{{limit}}"以降にしてください',
    max: '!!"{{limit}}"以前にしてください',
    isoDate: '!!有効なISO 8601形式の日付にしてください',
    timestamp: {
      javascript: '!!有効なtimestampまたはミリ秒単位の数値にしてください',
      unix: '!!有効なtimestampまたは秒数にしてください'
    },
    ref: '!!日付ではない"{{ref}}"を参照しています'
  },
  function: {
    base: '!!関数にしてください',
    arity: '!!{{n}}個の引数にしてください',
    minArity: '!!{{n}}個以上の引数にしてください',
    maxArity: '!!{{n}}個以下の引数にしてください'
  },
  object: {
    base: '!!オブジェクト型にしてください',
    child: '!!要素"{{!key}}"が次の理由により失敗しました：{{reason}}',
    min: '!!要素数を{{limit}}以上にしてください',
    max: '!!要素数を{{limit}}以下にしてください',
    length: '!!要素数を{{limit}}にしてください',
    allowUnknown: '!!許可されていない値です',
    with: '!!"{{peer}}"も同時に含めてください',
    without: '!!"{{peer}}"とは同時に含めないでください',
    missing: '!!{{peers}}のうち少なくとも一項目を含めてください',
    xor: '!!{{peers}}はいずれか一項目のみを含めてください',
    or: '!!{{peers}}のうち少なくとも一項目を含めてください',
    and: '!!{{present}}が存在する場合には{{missing}}が必須です',
    nand: '!!"{{main}}"は{{peers}}と同時には含めないでください',
    assert: '!!"{{ref}}"は次のバリデーションに失敗しました：{{message}}',
    rename: {
      multiple: '!!多重の改称を無効にしており、かつ他のキーがすでに"{{to}}"に改称されていたため、"{{from}}"を改称することができません',
      override: '!!キーの上書きを無効にしており、かつ"{{to}}"がすでに存在するため、"{{from}}"を改称することができません'
    },
    type: '!!"{{type}}"のインスタンスにしてください'
  },
  number: {
    base: '!!数値型にしてください',
    min: '!!{{limit}}以上にしてください',
    max: '!!{{limit}}以下にしてください',
    less: '!!{{limit}}未満にしてください',
    greater: '!!{{limit}}超にしてください',
    float: '!!floatまたはdoubleにしてください',
    integer: '!!整数型にしてください',
    negative: '!!負の数にしてください',
    positive: '!!正の数にしてください',
    precision: '!!小数点以下を{{limit}}桁までにしてください',
    ref: '!!数値型ではない"{{ref}}"を参照しています',
    multiple: '!!{{multiple}}の倍数にしてください'
  },
  string: {
    base: '!!文字列型にしてください',
    min: '!!{{limit}}文字以上にしてください',
    max: '!!{{limit}}文字以下にしてください',
    length: '!!{{limit}}文字にしてください',
    alphanum: '!!英数字のみにしてください',
    token: '!!英数字とアンダースコアのみにしてください',
    regex: {
      base: '!!値"{{!value}}"が次のパターンに一致している必要があります: {{pattern}}',
      name: '!!値"{{!value}}"が{{name}}パターンに一致していません'
    },
    email: '!!有効なメールアドレスにしてください',
    uri: '!!有効なURIにしてください',
    uriCustomScheme: '!!{{scheme}}パターンに一致するスキームを伴う有効なURLにしてください',
    isoDate: '!!有効なISO 8601形式の日付にしてください',
    guid: '!!有効なGUIDにしてください',
    hex: '!!16進文字列のみにしてください',
    hostname: '!!有効なホスト名にしてください',
    lowercase: '!!小文字のみにしてください',
    uppercase: '!!大文字のみにしてください',
    trim: '!!先頭または末尾を空白にしないでください',
    creditCard: '!!クレジットカード番号にしてください',
    ref: '!!数値型ではない"{{ref}}"を参照しています',
    ip: '!!有効なIPアドレス（CIDRは{{cidr}}）にしてください',
    ipVersion: '!!{{version}}のいずれかである有効なIPアドレス（CIDRは{{cidr}}）にしてください'
  }
};

module.exports = language;
