const Joi = require('joi');
const Validator = require('./Validator');
const language = require('./language');

Validator.setOption({
  abortEarly: false,
  language: language
});

module.exports = {
  Validator: Validator,
  Joi: Joi
};
