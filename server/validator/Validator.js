const Joi = require('joi');

/**
 * バリデータ
 * @class Validator
 */
class Validator {
  /**
   * @param {Object} validations バリデーション（joiのスキーマ）
   */
  constructor(validations) {
    this.validations = validations;
    this.errors = {};
  }

  /**
   * バリデータのオプションを設定する
   * @static
   * @param {Object} option オプション
   */
  static setOption(option) {
    this.prototype.option = option;
  }

  /**
   * バリデートを行う
   * @param {Object} data データ
   * @returns {{isValid: boolean, data: Object, errors: Object}}
   * <ul>
   * <li>isValid データが適切か
   * <li>data バリデートされたデータ
   * <li>errors エラー
   * </ul>
   */
  validate(data) {
    this.resetError();

    const result = Joi.validate(data, this.validations, this.option);

    if (result.error) {
      result.error.details.forEach((detail) => {
        this.setError(detail.path, detail.message);
      });
    }
    return {
      isValid: this.isValid(),
      data: result.value,
      errors: this.errors
    };
  }

  /**
   * データが適切か判定する
   * @returns {boolean} true: 適切、false: 不適切
   */
  isValid() {
    return (Object.keys(this.errors).length === 0);
  }

  /**
   * エラーをクリアする
   */
  resetError() {
    this.errors = {};
  }

  /**
   * エラーを設定する
   * @param {string} field フィールド
   * @param {string} message エラーメッセージ
   */
  setError(field, message) {
    this.errors[field] = this.errors[field] || [];
    this.errors[field].push(message);
  }
}

module.exports = Validator;
