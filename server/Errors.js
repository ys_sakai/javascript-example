/**
 * エラー
 * @class Errors
 */
class Errors {
  /**
   * エラーオブジェクトを生成する
   * @static
   * @param {string} name エラー名称
   * @param {number} status ステータスコード
   * @param {string} message メッセージ
   * @param {Object} data データ
   * @param {function} context コンテキスト関数
   * @returns {Error} エラーオブジェクト
   */
  static create(name, status, message, data, context) {
    const error = new Error(message || undefined);
    Error.captureStackTrace(error, context);

    error.name = name;
    error.status = status;
    error.data = data || null;
    error.isAppError = true;

    return error;
  }

  /**
   * Bad Request (400)エラー
   * @static
   * @param {string} [message] メッセージ
   * @param {Object} [data] データ
   * @returns {Error}
   */
  static badRequest(message, data) {
    return this.create('BadRequestError', 400, message, data, this.badRequest);
  }

  /**
   * Not Found (404)エラー
   * @static
   * @param {string} [message] メッセージ
   * @param {Object} [data] データ
   * @returns {Error}
   */
  static notFound(message, data) {
    message = message || '指定の情報が見つかりませんでした';
    return this.create('NotFoundError', 404, message, data, this.notFound);
  }

  /**
   * Database Error(500)
   * @static
   * @param {string} [message] メッセージ
   * @param {Object} [data] データ
   * @returns {Error}
   */
  static databaseError(message, data) {
    message = message || 'データベースでエラーが発生しました';
    const error = this.create('DatabaseError', 500, message, data, this.databaseError);
    error.isDBError = true;
    return error;
  }

  /**
   * Internal Server Error(500)
   * @static
   * @param {string} [message] メッセージ
   * @param {Object} [data] データ
   * @returns {Error}
   */
  static internalServerError(message, data) {
    message = message || 'サーバ内でエラーが発生しました';
    return this.create('InternalServerError', 500, message, data, this.internalServerError);
  }
}

module.exports = Errors;
