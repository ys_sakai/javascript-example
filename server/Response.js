/**
 * レスポンス
 * @class Response
 */
class Response {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.status ステータス（success|fail|error）
   * @param {Object|null} [params.data=null] データ
   * @param {string|null} [params.message=null] メッセージ
   */
  constructor(params) {
    const p = params || {};
    this.status = (p.status) ? p.status : 'error';
    this.data = (p.data) ? p.data : null;
    this.message = (p.message) ? p.message : null;
  }

  /**
   * JSON形式に変換する
   * @returns {Object}
   */
  toJSON() {
    return {
      status: this.status,
      data: this.data,
      message: this.message
    };
  }
}

module.exports = Response;
