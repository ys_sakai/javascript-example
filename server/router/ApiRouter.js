const Router = require('./Router');
const ApiResponse = require('../Response');
const Errors = require('../Errors');

/**
 * APIルータ
 * @class ApiRouter
 * @extends {Router}
 */
class ApiRouter extends Router {
  /**
   * @param {ModelManager} modelManager モデル管理
   */
  constructor(modelManager) {
    super();
    this.modelManager = modelManager;
  }

  /**
   * 各ルートを登録する
   */
  mountRoutes() {
    this.route('GET', '/:models', req => this.handleFind(req));
    this.route('POST', '/:models', req => this.handleCreate(req));
    this.route('GET', '/:models/:id', req => this.handleFindById(req));
    this.route('PUT', '/:models/:id', req => this.handleUpdate(req));
    this.route('DELETE', '/:models/:id', req => this.handleDelete(req));
  }

  /**
   * 検索処理のハンドラ
   * @param {Object} request リクエスト
   * @returns {Promise.<Object>}
   */
  handleFind(request) {
    const modelName = request.params.models;

    const Model = this.getModelClass(modelName);

    return Model.find()
      .then((models) => {
        const respense = new ApiResponse({
          status: 'success',
          data: models,
          message: `read ${Model.schemaName}`
        });
        return { response: respense };
      });
  }

  /**
   * 新規作成処理のハンドラ
   * @param {Object} request リクエスト
   * @returns {Promise.<Object>}
   */
  handleCreate(request) {
    const modelName = request.params.models;
    const data = request.body.data;

    const Model = this.getModelClass(modelName);

    return Model.create(data)
      .then((model) => {
        const respense = new ApiResponse({
          status: 'success',
          data: model,
          message: `create ${Model.schemaName}`
        });
        return { response: respense };
      });
  }

  /**
   * IDによる検索処理のハンドラ
   * @param {Object} request リクエスト
   * @returns {Promise.<Object>}
   */
  handleFindById(request) {
    const modelName = request.params.models;
    const id = request.params.id;

    const Model = this.getModelClass(modelName);

    return Model.findById(id)
      .then((model) => {
        const respense = new ApiResponse({
          status: 'success',
          data: model,
          message: `read ${Model.schemaName} by ${id}`
        });
        return { response: respense };
      });
  }

  /**
   * 更新処理のハンドラ
   * @param {Object} request リクエスト
   * @returns {Promise.<Object>}
   */
  handleUpdate(request) {
    const modelName = request.params.models;
    const id = request.params.id;
    const data = request.body.data;

    const Model = this.getModelClass(modelName);

    return Model.update(id, data)
      .then((model) => {
        const respense = new ApiResponse({
          status: 'success',
          data: model,
          message: `update ${Model.schemaName} by ${id}`
        });
        return { response: respense };
      });
  }

  /**
   * 削除処理のハンドラ
   * @param {Object} request リクエスト
   * @returns {Promise.<Object>}
   */
  handleDelete(request) {
    const modelName = request.params.models;
    const id = request.params.id;

    const Model = this.getModelClass(modelName);

    return Model.destroy(id)
      .then((model) => {
        const respense = new ApiResponse({
          status: 'success',
          data: model,
          message: `delete ${Model.schemaName} by ${id}`
        });
        return { response: respense };
      });
  }

  /**
   * モデルクラスを取得する
   * @param {string} modelName モデル名
   * @returns {Function} モデルクラス
   */
  getModelClass(modelName) {
    const Model = this.modelManager.get(modelName);
    if (!Model) {
      throw Errors.badRequest(`モデル:${modelName}は定義されていません`);
    }
    return Model;
  }
}

module.exports = ApiRouter;
