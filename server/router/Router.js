const express = require('express');
const router = express.Router();

/**
 * ルータ
 * @class Router
 */
class Router {
  constructor() {
    this.routes = [];
    this.mountRoutes();
  }

  /**
   * 各ルートを登録する
   */
  mountRoutes() {
    // サブクラスでオーバーライドする
  }

  /**
   * 指定のメソッド、パスに対して、ハンドラを紐づけて、ルートを登録する。
   * ハンドラはrequestを受け取り、Promise.<Object>を返却する。
   * @param {string} method HTTPメソッド（GET, POST, PUT, DELETE）
   * @param {string} path パス（Expressのフォーマット）
   * @param {function} handler ハンドラ
   */
  route(method, path, handler) {
    this.routes.push({
      method: method,
      path: path,
      handler: handler
    });
  }

  /**
   * 各ルートをExpressのルータに設定する
   * @returns {Object} ExpressのRouter
   */
  expressApp() {
    this.routes.forEach((route) => {
      switch (route.method) {
        case 'POST':
          router.post(route.path, makeExpressHandler(route.handler));
          break;
        case 'GET':
          router.get(route.path, makeExpressHandler(route.handler));
          break;
        case 'PUT':
          router.put(route.path, makeExpressHandler(route.handler));
          break;
        case 'DELETE':
          router.delete(route.path, makeExpressHandler(route.handler));
          break;
        default:
          throw new Error(`無効なHTTPメソッドです:  ${route.method}`);
      }
    });
    return router;
  }
}

/**
 * PromiseのハンドラからExpressのハンドラを作成する。
 * @param {function} promiseHandler Promiseのハンドラ
 * @returns {function} Expressのハンドラ
 */
function makeExpressHandler(promiseHandler) {
  return (request, response, next) => {
    try {
      promiseHandler(request)
        .then((result) => {
          response.json(result.response);
        })
        .catch((error) => {
          next(error);
        });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = Router;
