const ApiRouter = require('./ApiRouter');
const modelManager = require('../model');

const apiRouter = new ApiRouter(modelManager);

module.exports = {
  apiRouter: apiRouter
};
