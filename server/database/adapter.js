/**
 * アダプタ
 * @class Adapter
 */
class Adapter {
  /**
   * @abstract
   * @constructs
   * @param {Object} config DB設定
   */

  /**
   * クエリを実行する
   * @abstract
   * @param {string} query クエリ
   * @param {Array} values パラメータ
   * @returns {Promise.<Array.<Object>|number|Error>} クエリの実行結果
   */
  query() { notImplemented(); }
}

/**
 * トランザクション・アダプタ
 * @class TransactionAdaptera
 */
class TransactionAdapter {
  /**
   * @abstract
   * @constructs
   * @param {Object} config DB設定
   */

  /**
   * トランザクションを開始する
   * @abstract
   * @returns {Promise.<Object|Error>}
   */
  begin() { notImplemented(); }

  /**
   * トランザクションをコミットする
   * @abstract
   * @returns {Promise.<Object|Error>}
   */
  commit() { notImplemented(); }

  /**
   * トランザクションをロールバックする
   * @abstract
   * @returns {Promise.<Object|Error>}
   */
  rollback() { notImplemented(); }

  /**
   * トランザクション内でクエリを実行する
   * @abstract
   * @param {string} query クエリ
   * @param {Array} values パラメータ
   * @returns {Promise.<Array.<Object>|number|Error>} クエリの実行結果
   */
  query() { notImplemented(); }
}

/**
 * 未実装時にエラーとする
 */
function notImplemented() {
  throw new Error('not implemented');
}

module.exports = {
  Adapter: Adapter,
  TransactionAdapter: TransactionAdapter
};
