const sql = require('sql');

/**
 * クエリ生成
 * @class QueryGenerator
 */
class QueryGenerator {
  /**
   * @param {string} tableName テーブル名
   * @param {Array.<string>} columnNames カラム名
   */
  constructor(tableName, columnNames) {
    this.table = sql.define({
      name: tableName,
      columns: columnNames
    });
  }

  /**
   * SQLの方言を設定する
   * @static
   * @param {string} dialect SQLの方言（mssql, mysql, postgres, sqlite）
   */
  static setDialect(dialect) {
    sql.setDialect(dialect);
  }

  /**
   * SELECTクエリを生成する
   * @param {Object} params パラメータ
   * @param {Array.<string>} [params.columns] 取得するカラム
   * @param {Object} [params.where] 検索条件
   * @returns {{text: string, values: Array}} クエリ
   */
  select(params) {
    params = params || {};
    const columns = params.columns || this.table.star();
    const where = generateWhere(this.table, params.where);

    let query = this.table.select.apply(this.table, columns);
    query = query.from(this.table);
    if (where) {
      query = query.where.apply(query, where);
    }
    return query.toQuery();
  }

  /**
   * INSERTクエリを生成する
   * @param {Object} params パラメータ
   * @param {Object} params.data データ
   * @returns {{text: string, values: Array}} クエリ
   */
  insert(params) {
    params = params || {};
    const data = params.data;

    const query = this.table.insert(data);
    return query.toQuery();
  }

  /**
   * UPDATEクエリを生成する
   * @param {Object} params パラメータ
   * @param {Object} params.data データ
   * @param {Object} [params.where] 更新条件
   * @returns {{text: string, values: Array}} クエリ
   */
  update(params) {
    params = params || {};
    const data = params.data;
    const where = generateWhere(this.table, params.where);

    let query = this.table.update(data);
    if (where) {
      query = query.where.apply(query, where);
    }
    return query.toQuery();
  }

  /**
   * DELETEクエリを生成する
   * @param {Object} params パラメータ
   * @param {Object} [params.where] 削除条件
   * @returns {{text: string, values: Array}} クエリ
   */
  delete(params) {
    params = params || {};
    const where = generateWhere(this.table, params.where);

    let query = this.table.delete().from(this.table);
    if (where) {
      query = query.where.apply(query, where);
    }
    return query.toQuery();
  }
}

/**
 * WHERE句を生成する
 * @param {Object} table テーブル
 * @param {Object} [where] 条件
 * @returns {Array|null} クエリ
 */
function generateWhere(table, where) {
  if (!where) return null;

  const result = [];
  const keys = Object.keys(where);
  keys.forEach((key) => {
    if (!table[key]) return;
    result.push(table[key].equals(where[key]));
  });
  return result;
}

module.exports = QueryGenerator;
