const config = require('../../config');
const Database = require('./Database');
const Adapter = require('./postgres').Postgres;
const TxAdapter = require('./postgres').PostgresTransaction;
const QueryGenerator = require('./QueryGenerator');

const database = new Database(Adapter, TxAdapter, config.database);

QueryGenerator.setDialect('postgres');

module.exports = {
  database: database,
  QueryGenerator: QueryGenerator
};
