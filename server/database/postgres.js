const pg = require('pg');
const Adapter = require('./adapter').Adapter;
const TransactionAdapter = require('./adapter').TransactionAdapter;

/**
 * PostgreSQLアダプタ
 * @class Postgres
 * @extends {Adapter}
 */
class Postgres extends Adapter {
  /**
   * @param {Object} config DB設定
   */
  constructor(config) {
    super();
    this.pg = pg;
    this.config = config;
  }

  /**
   * クエリを実行する
   * @param {string} query クエリ
   * @param {Array} values パラメータ
   * @returns {Promise.<Array.<Object>|number|Error>} クエリの実行結果
   */
  query(query, values) {
    let conn = null;

    return createConnection(this.pg, this.config)
      .then((connection) => {
        conn = connection;
        return executeQuery(conn, query, values);
      })
      .then((result) => {
        releaseConnection(conn);
        return Promise.resolve(result);
      })
      .catch((error) => {
        destroyConnection(conn, error);
        return Promise.reject(error);
      });
  }
}

/**
 * PostgreSQLのトランザクション・アダプタ
 * @class PostgresTransaction
 * @extends {TransactionAdapter}
 */
class PostgresTransaction extends TransactionAdapter {
  /**
   * @param {Object} config DB設定
   */
  constructor(config) {
    super();
    this.pg = pg;
    this.config = config;
    this.conn = null;
  }

  /**
   * トランザクションを開始する
   * @returns {Promise.<Object|Error>}
   */
  begin() {
    return createConnection(this.pg, this.config)
      .then((connection) => {
        this.conn = connection;
        return this.query('BEGIN');
      })
      .then(() => Promise.resolve())
      .catch((error) => {
        destroyConnection(this.conn, error);
        return Promise.reject(error);
      });
  }

  /**
   * トランザクションをコミットする
   * @returns {Promise.<Object|Error>}
   */
  commit() {
    return this.query('COMMIT')
      .then(() => {
        releaseConnection(this.conn);
        return Promise.resolve();
      })
      .catch((error) => {
        destroyConnection(this.conn, error);
        return Promise.reject(error);
      });
  }

  /**
   * トランザクションをロールバックする
   * @returns {Promise.<Object|Error>}
   */
  rollback() {
    return this.query('ROLLBACK')
      .then(() => {
        releaseConnection(this.conn);
        return Promise.resolve();
      })
      .catch((error) => {
        destroyConnection(this.conn, error);
        return Promise.reject(error);
      });
  }

  /**
   * トランザクション内でクエリを実行する
   * @param {string} query クエリ
   * @param {Array} values パラメータ
   * @returns {Promise.<Array.<Object>|number|Error>} クエリの実行結果
   */
  query(query, values) {
    return executeQuery(this.conn, query, values);
  }
}

/**
 * DBに接続する
 * @param {Object} pg
 * @param {Object} config
 * @returns {Promise}
 */
function createConnection(pg, config) {
  return new Promise((resolve, reject) => {
    pg.connect(config, (error, client, done) => {
      if (error) {
        reject(error);
      } else {
        resolve({ client: client, done: done });
      }
    });
  });
}

/**
 * DBの接続を解放する
 * @param {Object} connection DB接続
 */
function releaseConnection(connection) {
  if (connection && connection.done) {
    connection.done();
    connection = null;
  }
}

/**
 * エラー時にDBの接続を破棄する
 * @param {Object} connection DB接続
 * @param {Error} error エラー
 */
function destroyConnection(connection, error) {
  if (connection && connection.done) {
    connection.done(error);
    connection = null;
  }
}

/**
 * クエリを実行する
 * @param {Object} connection DB接続
 * @param {string} query クエリ
 * @param {Array} values パラメータ
 * @returns {Promise.<Array.<Object>|number|Error>} クエリの実行結果
 */
function executeQuery(connection, query, values) {
  return new Promise((resolve, reject) => {
    connection.client.query(query, values, (error, data) => {
      if (error) {
        reject(error);
      } else {
        const result = convertQueryResult(data);
        resolve(result);
      }
    });
  });
}

/**
 * クエリの実行結果を返却用に変換する
 * @param {Object} data
 * @returns {Array.<Object>|number}
 */
function convertQueryResult(data) {
  if (!data) return null;
  switch (data.command) {
    case 'DELETE':
    case 'UPDATE':
      return data.rowCount;
    default:
      return data.rows;
  }
}

module.exports = {
  Postgres: Postgres,
  PostgresTransaction: PostgresTransaction
};
