const Errors = require('../Errors');

/**
 * トランザクション
 * @class Transaction
 */
class Transaction {
  /**
   * @param {TransactionAdapter} txAdapter トランザクション・アダプタ
   */
  constructor(txAdapter) {
    this.txAdapter = txAdapter;
    this.isTransacting = false;
  }

  /**
   * トランザクションを開始する
   * @returns {Promise.<Object|Error>}
   */
  begin() {
    return this.txAdapter.begin()
      .then(() => {
        this.isTransacting = true;
      });
  }

  /**
   * トランザクションをコミットする
   * @returns {Promise.<Object|Error>}
   */
  commit() {
    if (!this.isTransacting) return Promise.resolve();
    return this.txAdapter.commit()
      .then(() => {
        this.isTransacting = false;
      });
  }

  /**
   * トランザクションをロールバックする
   * @returns {Promise.<Object|Error>}
   */
  rollback() {
    if (!this.isTransacting) return Promise.resolve();
    this.isTransacting = false;
    return this.txAdapter.rollback();
  }
}

/**
 * データベース
 * @class Database
 */
class Database {
  /**
   * @param {Function} Adapter アダプタ
   * @param {Function} TxAdapter トランザクション・アダプタ
   * @param {Object} config DB設定
   */
  constructor(Adapter, TxAdapter, config) {
    this.adapter = new Adapter(config);
    this.TxAdapter = TxAdapter;
    this.config = config;
  }

  /**
   * クエリを実行する。
   * トランザクション内で実行する場合には第三引数を指定する。
   * @param {string} query クエリ
   * @param {Array} values パラメータ
   * @param {Transaction} [transaction] トランザクション
   * @returns {Promise.<Array.<Object>|number|Error>} クエリの実行結果
   */
  query(query, values, transaction) {
    const adapter = (!transaction) ? this.adapter : transaction.txAdapter;

    return adapter.query(query, values)
      .catch((error) => this.handleQueryError(error, query, values, transaction));
  }

  /**
   * クエリ実行時のエラーを処理する
   * @param {Error} error エラー
   * @param {string} query クエリ
   * @param {Array} values パラメータ
   * @param {Transaction} [transaction] トランザクション
   * @returns {Promise.<Error>}
   */
  handleQueryError(error, query, values, transaction) {
    const message = 'クエリの実行に失敗しました';
    const data = { query: query, values: values, error: error };
    if (!transaction) {
      console.error(message, data);
    }
    return Promise.reject(Errors.databaseError(message, data));
  }

  /**
   * ハンドラ内の処理をトランザクション処理として実行する。
   * ハンドラはTransactionを受け取り、Promise.<Object>を返却する。
   *
   * @example
   * db.transaction((tx) => {
   *   return db.query(query1, values1, tx)
   *     .then(() => db.query(query2, values2, tx));
   *   // ⇒ query1、query2がトランザクション内で実行され、コミットされる。
   *   // 例外発生時はロールバックされる。
   * })
   * .catch((error) => {})
   *
   * @param {function} promiseHandler Promiseのハンドラ
   * @returns {Promise.<Object|Error>}
   */
  transaction(promiseHandler) {
    const txAdapter = new this.TxAdapter(this.config);
    const transaction = new Transaction(txAdapter);

    return transaction.begin()
      .then(() => this.handleTransaction(promiseHandler, transaction))
      .catch((error) => this.handleTransactionError(error, transaction));
  }

  /**
   * トランザクションを処理する
   * @param {function} promiseHandler Promiseのハンドラ
   * @param {Transaction} transaction トランザクション
   * @returns {Promise.<Object|Error>}
   */
  handleTransaction(promiseHandler, transaction) {
    return new Promise((resolve, reject) => {
      try {
        promiseHandler(transaction)
          .then(() => transaction.commit())
          .then(() => resolve())
          .catch((error) => reject(error));
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * トランザクション実行時のエラーを処理する
   * @param {Error} error エラー
   * @param {Transaction} transaction トランザクション
   * @returns {Promise.<Error>}
   */
  handleTransactionError(error, transaction) {
    transaction.rollback();

    const message = 'トランザクション処理に失敗しました';
    console.error(message, error);
    return Promise.reject(Errors.databaseError(message, error));
  }
}

module.exports = Database;
