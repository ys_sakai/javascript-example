const express = require('express');
const path = require('path');
const logger = require('morgan');
const methodOverride = require('method-override');
// const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const apiRouter = require('./router').apiRouter;
const Errors = require('./Errors');
const ErrorHandler = require('./ErrorHandler');

/**
 * サーバ
 * @class Server
 */
class Server {
  constructor() {
    this.app = express();
    this.publicPath = path.join(__dirname, '../public');
    this.useMiddlewares();
  }

  /**
   * 指定のポートで接続を待ち受ける
   * @param {number} port ポート
   */
  listen(port) {
    this.app.listen(port);
    console.log('server is listening on port %d, in %s mode', port, this.app.settings.env);
  }

  /**
   * Expressにミドルウェアを登録する
   */
  useMiddlewares() {
    this.app.use(logger('dev'));
    this.app.use(this.parser());

    this.app.use('/', express.static(this.publicPath));
    this.app.use('/api', apiRouter.expressApp());

    this.app.use(this.notFound());
    this.app.use(this.handleError());
  }

  /**
   * パーサ
   * @returns {Object} ExpressのRouter
   */
  parser() {
    const parser = express.Router();

    const reviver = (key, value) => {
      if (typeof(value) === 'string' &&
        value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/)) {
        return new Date(Date.parse(value));
      }
      return value;
    };

    parser.use(bodyParser.json({ reviver: reviver }));
    parser.use(bodyParser.urlencoded({ extended: false }));
    // app.use(cookieParser());
    parser.use(methodOverride('X-HTTP-Method-Override'));

    return parser;
  }

  /**
   * Not Found (404)エラー
   * @returns {function} Expressのミドルウェア
   */
  notFound() {
    return (request, response, next) => {
      next(Errors.notFound());
    };
  }

  /**
   * エラー処理
   * @returns {function} Expressのエラー用ミドルウェア
   */
  handleError() {
    return ErrorHandler.expressApp();
  }
}

module.exports = Server;
