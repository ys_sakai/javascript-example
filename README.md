# javascript-example
Node.js + jQueryを使用したJavaScriptのSPA構築用のサンプル

## Description
2015年～2016年にかけて、SPAの学習のために作成したもの。  
今となっては、フロントエンドがjQueryなことやライブラリのバージョンが若干古いことが気になるが、当時は大変勉強になった。

- Node.js
- Express
- jQuery
- webpack
- Babel

