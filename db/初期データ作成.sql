﻿-- サンプル（samples）初期データ作成
INSERT 
INTO samples(first_name, last_name, sex, job_category, certificate, remarks) 
VALUES ('シスラボ', '太郎', '01', '04', 6, 'テスト
テスト');
INSERT 
INTO samples(first_name, last_name, sex, job_category, certificate, remarks) 
VALUES ('シスラボ','花子','02','02',2,'テストテスト');

-- コードマスタ（master_codes）初期データ作成
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('001','01','性別','男性');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('001','02','性別','女性');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','01','職種','マーケティング');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','02','職種','セールス');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','03','職種','コンサルタント');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','04','職種','ITアーキテクト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','05','職種','プロジェクトマネジメント');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','06','職種','ITスペシャリスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','07','職種','アプリケーションスペシャリスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','08','職種','ソフトウェアデベロップメント');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','09','職種','カスタマーサービス');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','10','職種','ITサービスマネジメント');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('002','11','職種','エデュケーション');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','01','資格','基本情報技術者');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','02','資格','応用情報技術者');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','03','資格','ITストラテジスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','04','資格','システムアーキテクト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','05','資格','プロジェクトマネージャ');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','06','資格','ネットワークスペシャリスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','07','資格','データベーススペシャリスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','08','資格','エンベデッドシステムスペシャリスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','09','資格','情報セキュリティスペシャリスト');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','10','資格','ITサービスマネージャ');
INSERT INTO master_codes(group_code, code, group_name, name) VALUES ('003','11','資格','システム監査技術者');

