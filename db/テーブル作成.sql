﻿-- サンプル（samples）
-- DROP TABLE public.samples;
CREATE TABLE public.samples
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(), -- ID
  first_name character varying(20), -- 姓
  last_name character varying(20), -- 名
  sex character(2), -- 性別
  job_category character(2), -- 職種
  certificate integer, -- 資格
  remarks character varying(400), -- 備考
  created_at timestamp with time zone DEFAULT now(), -- 作成日時
  updated_at timestamp with time zone DEFAULT now(), -- 更新日時
  CONSTRAINT samples_pkey1 PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE public.samples
  IS 'サンプル';
COMMENT ON COLUMN public.samples.id IS 'ID';
COMMENT ON COLUMN public.samples.first_name IS '姓';
COMMENT ON COLUMN public.samples.last_name IS '名';
COMMENT ON COLUMN public.samples.sex IS '性別';
COMMENT ON COLUMN public.samples.job_category IS '職種';
COMMENT ON COLUMN public.samples.certificate IS '資格';
COMMENT ON COLUMN public.samples.remarks IS '備考';
COMMENT ON COLUMN public.samples.created_at IS '作成日時';
COMMENT ON COLUMN public.samples.updated_at IS '更新日時';

-- コードマスタ（master_codes）
-- DROP TABLE public.master_codes;
CREATE TABLE public.master_codes
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(), -- ID
  group_code character(3) NOT NULL, -- グループCD
  code character(2) NOT NULL, -- コード
  group_name character varying(20), -- グループ名称
  name character varying(40), -- 名称
  CONSTRAINT master_codes_pkey PRIMARY KEY (id),
  CONSTRAINT master_codes_group_code_code_key UNIQUE (group_code, code)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE public.master_codes
  IS 'コードマスタ';
COMMENT ON COLUMN public.master_codes.id IS 'ID';
COMMENT ON COLUMN public.master_codes.group_code IS 'グループCD';
COMMENT ON COLUMN public.master_codes.code IS 'コード';
COMMENT ON COLUMN public.master_codes.group_name IS 'グループ名称';
COMMENT ON COLUMN public.master_codes.name IS '名称';

