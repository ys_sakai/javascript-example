const Event = require('../Event');
const Util = require('../Util');

/**
 * ステートマシン
 * @class StateMachine
 */
class StateMachine {
  /**
   * コントローラを追加する
   * @param {AbstractController} controller コントローラのインスタンス
   */
  add(controller) {
    this.on('change', (current) => {
      if (controller === current) {
        controller.activate();
      } else {
        controller.deactivate();
      }
    });

    controller.active = () => {
      this.trigger('change', controller);
    };
  }
}

Util.mixin(StateMachine, Event);

module.exports = StateMachine;

