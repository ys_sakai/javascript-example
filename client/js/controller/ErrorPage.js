const AbstractController = require('./AbstractController');
const View = require('../view/ErrorPage');

/**
 * エラーページ
 * @class ErrorPage
 * @extends {AbstractController}
 */
class ErrorPage extends AbstractController {
  constructor() {
    super();

    this.view = new View('error');
  }

  /**
   * 初期化する
   */
  init() {
    this.view.init();
    this._attachEvent();
  }

  /**
   * アクティブにする
   */
  activate() {
    this.view.show();
    console.log('ErrorPage:active');
  }

  /**
   * パッシブにする
   */
  deactivate() {
    this.view.hide();
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.view.on('reload', () => this._reload());
  }

  /**
   * 再読み込みする
   */
  _reload() {
    window.location.reload();
  }

  /**
   * ビューにメッセージを設定する
   * @param {Error} error エラー
   */
  set(error) {
    this.view.set(error);

    if (!error.isAppError) {
      console.log(error);
    }
  }
}

module.exports = ErrorPage;
