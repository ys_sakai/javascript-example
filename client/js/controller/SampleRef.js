const AbstractController = require('./AbstractController');
const Model = require('../model/Sample');
const View = require('../view/SampleRef');

/**
 * サンプル参照
 * @class SampleRef
 * @extends {AbstractController}
 */
class SampleRef extends AbstractController {
  constructor() {
    super();

    this.view = new View('ref');

    this.model = null;
  }

  /**
   * 初期化する
   */
  init() {
    this.view.init();
    this._attachEvent();
  }

  /**
   * アクティブにする
   */
  activate() {
    this.view.show();
    console.log('SampleRef:active');
  }

  /**
   * パッシブにする
   */
  deactivate() {
    this.view.hide();
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.view.on('edit', () => this.trigger('edit', this.model.id));
    this.view.on('back', () => this.trigger('back'));
  }

  /**
   * ビューにデータを設定する
   * @param {string} [id] ID
   */
  set(id) {
    this.model = Model.findById(id);
    this.view.set(this.model);
    console.log(this.model.data);
  }
}

module.exports = SampleRef;
