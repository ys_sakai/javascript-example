const AbstractController = require('./AbstractController');
const Model = require('../model/Sample');
const Master = require('../model/CodeMaster');
const View = require('../view/SampleList');

/**
 * サンプル一覧
 * @class SampleList
 * @extends {AbstractController}
 */
class SampleList extends AbstractController {
  constructor() {
    super();

    this.view = new View('list');
  }

  /**
   * 初期化する
   */
  init() {
    this.view.init();
    this._attachEvent();
  }

  /**
   * アクティブにする
   */
  activate() {
    this.view.show();
    console.log('SampleList:active');
  }

  /**
   * パッシブにする
   */
  deactivate() {
    this.view.hide();
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    Model.on('change', () => this.view.refresh(Model));
    Master.on('change', () => this.view.refresh(Model));

    this.view.on('click:new', () => this.trigger('new'));
    this.view.on('click:copy', (id) => this._copy(id));
    this.view.on('click:delete', (id) => this._delete(id));
    this.view.on('click:refer', (id) => this.trigger('refer', id));
  }

  /**
   * コピーする
   * @param {string} id ID
   */
  _copy(id) {
    Model.copy(id).save()
      .catch((error) => {
        this.trigger(error);
      });
  }

  /**
   * 削除する
   * @param {string} id ID
   */
  _delete(id) {
    Model.findById(id).destroy()
      .catch((error) => {
        this.trigger(error);
      });
  }
}

module.exports = SampleList;
