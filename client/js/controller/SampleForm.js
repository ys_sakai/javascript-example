const AbstractController = require('./AbstractController');
const Model = require('../model/Sample');
const View = require('../view/SampleForm');

/**
 * サンプルフォーム
 * @class SampleForm
 * @extends {AbstractController}
 */
class SampleForm extends AbstractController {
  constructor() {
    super();

    this.view = new View('form');

    this.model = null;
  }

  /**
   * 初期化する
   */
  init() {
    this.view.init();
    this._attachEvent();
  }

  /**
   * アクティブにする
   */
  activate() {
    this.view.show();
    this.view.refresh();
    console.log('SampleForm:active');
  }

  /**
   * パッシブにする
   */
  deactivate() {
    this.view.hide();
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.view.on('save', () => this.save());
    this.view.on('cancel', () => this.trigger('cancel', this.model.isNew()));
  }

  /**
   * ビューにデータを設定する
   * @param {string} [id] ID
   */
  set(id) {
    this.model = !id ? Model.create() : Model.findById(id);
    this.view.set(this.model);
    console.log(this.model.data);
  }

  /**
   * ビューのデータを保存する
   */
  save() {
    const data = this.view.get();
    this.model.set(data);

    this.model.save()
      .then(() => {
        this.trigger('save');
        console.log(Model.models);
      })
      .catch((error) => {
        this.trigger('error', error);
      });
  }
}

module.exports = SampleForm;
