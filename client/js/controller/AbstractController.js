const Util = require('../Util');
const Event = require('../Event');

/**
 * 抽象コントローラ
 * @class AbstractController
 */
class AbstractController {
  /**
   * 初期化する
   */
  init() {}
}

Util.mixin(AbstractController, Event);

module.exports = AbstractController;
