const AbstractController = require('./AbstractController');
const StateMachine = require('./StateMachine');
const SampleForm = require('./SampleForm');
const SampleRef = require('./SampleRef');
const SampleList = require('./SampleList');
const ErrorPage = require('./ErrorPage');

const SampleModel = require('../model/Sample');
const CodeMaster = require('../model/CodeMaster');

/**
 * サンプル
 * @class Sample
 * @extends {AbstractController}
 */
class Sample extends AbstractController {
  constructor() {
    super();

    this.sm = new StateMachine();

    this.form = new SampleForm();
    this.ref = new SampleRef();
    this.list = new SampleList();
    this.error = new ErrorPage();
  }

  /**
   * 初期化する
   */
  init() {
    this._initChild();
    this._attachEvent();

    this.list.active();

    this._loadData()
      .catch((error) => {
        this._handleError(error);
      });
  }

  /**
   * データを読み込む
   * @returns {Promise}
   */
  _loadData() {
    return Promise.all([
      CodeMaster.fetch(),
      SampleModel.fetch()
    ]);
  }

  /**
   * 子クラスを初期化する
   */
  _initChild() {
    this.form.init();
    this.ref.init();
    this.list.init();
    this.error.init();

    this.sm.add(this.form);
    this.sm.add(this.ref);
    this.sm.add(this.list);
    this.sm.add(this.error);
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.list.on('new', () => this._onNew());
    this.list.on('refer', (id) => this._onRefer(id));
    this.list.on('error', (error) => this._handleError(error));

    this.ref.on('edit', (id) => this._onEdit(id));
    this.ref.on('back', () => this._onBack());

    this.form.on('save', () => this._onSave());
    this.form.on('cancel', (isNew) => this._onCancel(isNew));
    this.form.on('error', (error) => this._handleError(error));
  }

  /**
   * 「新規」時の処理
   */
  _onNew() {
    this.form.active();
    this.form.set();
  }

  /**
   * 「参照」時の処理
   * @param {string} id ID
   */
  _onRefer(id) {
    this.ref.active();
    this.ref.set(id);
  }

  /**
   * 「編集」時の処理
   * @param {string} id ID
   */
  _onEdit(id) {
    this.form.active();
    this.form.set(id);
  }

  /**
   * 「戻る」時の処理
   */
  _onBack() {
    this.list.active();
  }

  /**
   * 「保存」時の処理
   */
  _onSave() {
    this.list.active();
  }

  /**
   * 「キャンセル」時の処理
   * @param {boolean} isNew true: 新規、false: 更新
   */
  _onCancel(isNew) {
    if (isNew) {
      this.list.active();
    } else {
      this.ref.active();
    }
  }

  /**
   * エラーを処理する
   * @param {Error} error エラー
   */
  _handleError(error) {
    this.error.active();
    this.error.set(error);
  }
}

module.exports = Sample;
