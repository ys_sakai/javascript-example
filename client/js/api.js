/**
 * API
 * @class Api
 */
class Api {
  /**
   * モデルをサーバサイドと同期する
   * @param {string} method メソッド
   * @param {Function|Model} model モデルのコンストラクタまたはインスタンス
   * @returns {Promise.<Array.<Object>|Object|Error>}
   */
  sync(method, model) {
    switch (method) {
      case 'read':
        return this.get(model.url).then((json) => json.data);
      case 'create':
        return this.post(model.url, { data: model }).then((json) => json.data);
      case 'update':
        return this.put(model.url, { data: model }).then((json) => json.data);
      case 'delete':
        return this.delete(model.url).then((json) => json.data);
      default:
        throw new Error('不正なメソッドです');
    }
  }

  /**
   * リソースを取得する
   * @param {string} url リソースのURL
   * @returns {Promise.<Object|Error>}
   */
  get(url) {
    return this.fetch(url, {
      method: 'GET'
    });
  }

  /**
   * リソースを作成する
   * @param {string} url リソースのURL
   * @param {Object} data データ
   * @returns {Promise.<Object|Error>}
   */
  post(url, data) {
    return this.fetch(url, {
      method: 'POST',
      body: JSON.stringify(data)
    });
  }

  /**
   * リソースを更新する
   * @param {string} url リソースのURL
   * @param {Object} data データ
   * @returns {Promise.<Object|Error>}
   */
  put(url, data) {
    return this.fetch(url, {
      method: 'PUT',
      body: JSON.stringify(data)
    });
  }

  /**
   * リソースを削除する
   * @param {string} url リソースのURL
   * @returns {Promise.<Object|Error>}
   */
  delete(url) {
    return this.fetch(url, {
      method: 'DELETE'
    });
  }

  /**
   * リソースのfetchを行う
   * @param {string} url リソースのURL
   * @param {Object} [option] リクエストのオプション
   * @returns {Promise.<Object|Error>}
   */
  fetch(url, option) {
    option = option || {};

    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const init = {
      method: option.method || 'GET',
      headers: headers
    };
    if (option.body) init.body = option.body;

    return fetch(url, init)
      .then((response) => this.handleResponse(response))
      .catch((error) => this.handleError(error));
  }

  /**
   * レスポンスを処理する
   * @param {Response} response レスポンス
   * @returns {Promise.<Object>}
   */
  handleResponse(response) {
    return this.parseJSON(response)
      .then((json) => {
        if (!response.ok) {
          throw Api.statusError(response, json);
        }
        return Promise.resolve(json);
      });
  }

  /**
   * レスポンスのJSONをパースする
   * @param {Response} response レスポンス
   * @returns {Promise.<Object>}
   */
  parseJSON(response) {
    return response.json()
      .catch((error) => {
        console.log(error);
        throw Api.parseError();
      });
  }

  /**
   * エラーを処理する
   * @param {Error} error エラー
   * @returns {Promise.<Error>}
   */
  handleError(error) {
    console.log(error);

    if (error.isStatusError || error.isParseError) {
      return Promise.reject(error);
    } else {
      return Promise.reject(Api.networkError());
    }
  }

  /**
   * レスポンス失敗（200-299以外のステータス）によるエラー
   * @static
   * @param {Response} response レスポンス
   * @param {Object} json JSON
   * @returns {Error}
   */
  static statusError(response, json) {
    const error = new Error(json.message);
    error.status = response.status;
    error.data = json.data;
    error.isStatusError = true;
    error.isAppError = true;
    return error;
  }

  /**
   * JSONパース失敗によるエラー
   * @static
   * @returns {Error}
   */
  static parseError() {
    const error = new Error('JSON形式のレスポンスを取得できませんでした');
    error.isParseError = true;
    error.isAppError = true;
    return error;
  }

  /**
   * 通信失敗によるエラー
   * @static
   * @returns {Error}
   */
  static networkError() {
    const error = new Error('通信エラーが発生しました');
    error.isNetworkError = true;
    error.isAppError = true;
    return error;
  }
}

module.exports = new Api();
