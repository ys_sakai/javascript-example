const $ = require('jquery');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * エスケープマップ
   * @type {Object}
   */
  escapeMap: {
    '&': '&amp;',
    "'": '&#x27;',
    '`': '&#x60;',
    '"': '&quot;',
    '<': '&lt;',
    '>': '&gt;'
  }
};

/**
 * ユーティリティ
 * @class Util
 */
class Util {
  /**
   * ランダムなIDを生成する
   * @static
   * @returns {string} ID
   */
  static generateGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = Math.random() * 16 | 0;
      const v = (c === 'x') ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  /**
   * オブジェクトをコピーする
   * @static
   * @param {Object} obj コピー元オブジェクト
   * @param {boolean} [isDeep=true] true: ディープコピー、false: シャローコピー
   * @returns {Object} コピーオブジェクト
   */
  static copy(obj, isDeep) {
    if (isDeep === undefined) isDeep = true;
    return $.extend(isDeep, {}, obj);
  }

  /**
   * 文字列をエスケープする。
   * 引数に文字列以外の値が渡された場合、文字列に変換してエスケープする。
   * @static
   * @param {string|*} str 文字列
   * @returns {string}
   */
  static escapeHtml(str) {
    str = (str === null || str === undefined) ? '' : String(str);

    const regex = this._makeRegex(config.escapeMap);
    return str.replace(regex, (match) => config.escapeMap[match]);
  }

  /**
   * マップのkeyから正規表現オブジェクトを生成する
   * @static
   * @param {Object} map マップ
   * @returns {RegExp}
   */
  static _makeRegex(map) {
    let pattern = '[';
    const keys = Object.keys(map);
    keys.forEach((key) => {
      pattern += key;
    });
    pattern += ']';
    return new RegExp(pattern, 'g');
  }

  /**
   * 文字列内の改行コードを\nに統一する
   * @static
   * @param {string} str 文字列
   * @returns {srting}
   */
  static unityLinefeedCode(str) {
    str = str.replace(/\r\n/g, '\n');
    str = str.replace(/\r/g, '\n');
    return str;
  }

  /**
   * ミックスインを行う
   * @static
   * @param {Object} baseClass クラス
   * @param {Object} target ミックスイン対象
   */
  static mixin(baseClass, target) {
    const targetObj = (typeof(target) === 'function') ? target.prototype : target;

    for (const key of Reflect.ownKeys(targetObj)) {
      if (key === 'constructor') continue;

      const descriptor = Object.getOwnPropertyDescriptor(targetObj, key);
      descriptor.configurable = true;
      descriptor.enumerable = false;
      if ({}.hasOwnProperty.call(descriptor, 'writable')) descriptor.writable = true;

      Object.defineProperty(baseClass.prototype, key, descriptor);
    }
  }
}

module.exports = Util;
