const Const = require('../Const');
const AbstractView = require('./AbstractView');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * 要素の設定
   * @type {Object}
   */
  els: {
    table: {
      type: Const.ElementType.TABLE,
      className: Const.Class.GRID
    },
  }
};

/**
 * グリッド
 * @class Grid
 * @extends {AbstractView}
 */
class Grid extends AbstractView {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {Array.<Object>} params.colModel 列モデル
   * @param {Array.<Object>} params.dataModel データモデル
   */
  constructor(params) {
    const _params = params || {};

    super(_params.id);

    config.els.table.id = _params.id;

    this.colModel = _params.colModel || [];
    this.dataModel = _params.dataModel || [];

    this.selectedData = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init(config);

    this._attachEvent();
    this.refresh();
  }

  /**
   * 更新する
   */
  refresh() {
    const cells = this._getCellData();
    this.els.table.create(cells);
    this.trigger('refresh');
  }

  /**
   * データ行を選択する
   * @param {number} dataIndex 選択行のインデックス
   */
  selectRow(dataIndex) {
    this.selectedData = this.dataModel[dataIndex];

    this.els.table.eachRow(($row, rowIndex) => {
      $row.removeClass(Const.Class.GRID_IS_SELECTED);
      if (rowIndex !== (dataIndex + 1)) return;
      $row.addClass(Const.Class.GRID_IS_SELECTED);
    }, this);

    this.trigger('select', this.selectedData, dataIndex);
  }

  /**
   * 列モデルを設定する
   * @param {Array.<Object>} colModel 列モデル
   */
  setColModel(colModel) {
    this.colModel = colModel || [];
  }

  /**
   * データモデルを設定する
   * @param {Array.<Object>} dataModel データモデル
   */
  setDataModel(dataModel) {
    this.dataModel = dataModel || [];
  }

  /**
   * 選択行のデータを取得する
   * @returns {Object} データ
   */
  getSelectedData() {
    return this.selectedData;
  }

  /**
   * セルのデータを取得する
   * @returns {Array.<Array.<Object>>} テーブルのセル
   */
  _getCellData() {
    const header = this._getHeaderRow();
    const len = this.dataModel.length;
    const data = (len === 0) ? this._getNoDataRows() : this._getDataRows();
    data.unshift(header);
    return data;
  }

  /**
   * テーブルのヘッダー行を取得する
   * @returns {Array.<Object>}
   */
  _getHeaderRow() {
    const cols = [];
    this.colModel.forEach((col) => {
      if (col.hidden) return;
      cols.push({ text: col.name, width: col.width, header: true });
    });
    return cols;
  }

  /**
   * テーブルのデータ行を取得する
   * @returns {Array.<Array.<Object>>}
   */
  _getDataRows() {
    const rows = [];
    // 行
    this.dataModel.forEach((data) => {
      const cols = [];
      // 列
      this.colModel.forEach((col) => {
        if (col.hidden) return;
        let value = data[col.key];
        value = (value !== undefined) ? value : '';
        cols.push({ text: value });
      });
      rows.push(cols);
    });
    return rows;
  }

  /**
   * データが存在しない旨を示すデータ行を取得する
   * @returns {Array.<Array.<Object>>}
   */
  _getNoDataRows() {
    const rows = [];
    const cols = [];
    const len = this._getColLength();
    const msg = 'データがありません';
    cols.push({ text: msg, colspan: len });
    rows.push(cols);
    return rows;
  }

  /**
   * テーブルの列数を取得する
   * @returns {number}
   */
  _getColLength() {
    let count = 0;
    this.colModel.forEach((col) => {
      if (col.hidden) return;
      count++;
    });
    return count;
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.on('refresh', () => {
      this._attachTableEvent();
    });

    this.on('click:row', (dataIndex) => {
      this.selectRow(dataIndex);
    });
  }

  /**
   * テーブルにイベントを設定する
   */
  _attachTableEvent() {
    if (this.dataModel.length === 0) return;
    this.els.table.eachRow(($row, rowIndex) => {
      if (rowIndex === 0) return;
      $row.on(
        {
          click: this._onClickDataRow.bind(this),
          dblclick: this._onDblClickDataRow.bind(this),
          mouseenter: this._onMouseEnterDataRow.bind(this),
          mouseleave: this._onMouseLeaveDataRow.bind(this)
        },
        {
          $row: $row,
          rowIndex: rowIndex
        }
      );
    }, this);
  }

  /**
   * データ行クリック時の処理
   * @param {Object} event
   */
  _onClickDataRow(event) {
    const dataIndex = event.data.rowIndex - 1;
    this.trigger('click:row', dataIndex);
  }

  /**
   * データ行ダブルクリック時の処理
   * @param {any} event
   */
  _onDblClickDataRow(event) {
    const dataIndex = event.data.rowIndex - 1;
    this.trigger('dblclick:row', dataIndex);
  }

  /**
   * データ行にマウスが入った際の処理
   * @param {Object} event
   */
  _onMouseEnterDataRow(event) {
    const $row = event.data.$row;
    $row.addClass(Const.Class.GRID_IS_HOVER);
  }

  /**
   * データ行からマウスが出た際の処理
   * @param {Object} event
   */
  _onMouseLeaveDataRow(event) {
    const $row = event.data.$row;
    $row.removeClass(Const.Class.GRID_IS_HOVER);
  }
}

module.exports = Grid;
