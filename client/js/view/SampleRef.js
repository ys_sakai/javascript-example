const Const = require('../Const');
const AbstractView = require('./AbstractView');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * 要素の設定
   * @type {Object}
   */
  els: {
    fullName: {
      id: 'ref-fullName',
      type: Const.ElementType.TEXT,
      labelName: '氏名'
    },
    sex: {
      id: 'ref-sex',
      type: Const.ElementType.TEXT,
      labelName: '性別'
    },
    jobCategory: {
      id: 'ref-jobCategory',
      type: Const.ElementType.TEXT,
      labelName: '職種'
    },
    certificate: {
      id: 'ref-certificate',
      type: Const.ElementType.TEXT,
      labelName: '資格'
    },
    remarks: {
      id: 'ref-remarks',
      type: Const.ElementType.TEXT,
      labelName: '備考'
    },
    editButton: {
      id: 'ref-edit-button',
      type: Const.ElementType.BUTTON,
      text: '編　集',
      width: 90
    },
    backButton: {
      id: 'ref-back-button',
      type: Const.ElementType.BUTTON,
      text: '戻　る',
      width: 90
    }
  }
};

/**
 * サンプル参照 ビュー
 * @class SampleRef
 * @extends {AbstractView}
 */
class SampleRef extends AbstractView {
  /**
   * 初期化する
   */
  init() {
    super.init(config);

    this._attachEvent();
  }

  /**
   * データを設定する
   * @param {Model} model モデルのインスタンス
   */
  set(model) {
    const data = this._getModelData(model);
    super.set(data);
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.els.editButton.on('click', () => this.trigger('edit'));
    this.els.backButton.on('click', () => this.trigger('back'));
  }

  /**
   * ビューに表示するモデルのデータを取得する
   * @param {Model} model モデルのインスタンス
   * @returns {Object} データ
   */
  _getModelData(model) {
    return {
      fullName: model.get('full_name'),
      sex: model.get('sex', true),
      jobCategory: model.get('job_category', true),
      certificate: model.get('certificate', true),
      remarks: model.get('remarks')
    };
  }
}

module.exports = SampleRef;
