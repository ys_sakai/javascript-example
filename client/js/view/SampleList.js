const Const = require('../Const');
const Model = require('../model/Sample');
const AbstractView = require('./AbstractView');
const Grid = require('./Grid');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * 要素の設定
   * @type {Object}
   */
  els: {
    newButton: {
      id: 'list-new-button',
      type: Const.ElementType.BUTTON,
      text: '新規',
      width: 60
    },
    copyButton: {
      id: 'list-copy-button',
      type: Const.ElementType.BUTTON,
      text: 'コピー',
      width: 60
    },
    deleteButton: {
      id: 'list-delete-button',
      type: Const.ElementType.BUTTON,
      text: '削除',
      width: 60
    },
    referButton: {
      id: 'list-refer-button',
      type: Const.ElementType.BUTTON,
      text: '参照',
      width: 60
    }
  },
  /**
   * グリッドの設定
   * @type {Object}
   */
  grid: {
    id: 'list-grid',
    /**
     * 列モデル
     * @type {Array.<Object>}
     */
    colModel: [
      {
        key: 'id',
        name: 'ID',
        hidden: true
      },
      {
        key: 'fullName',
        name: '氏名',
        width: 200
      },
      {
        key: 'jobCategory',
        name: '職種',
        width: 250
      }
    ]
  }
};

/**
 * サンプル一覧 ビュー
 * @class SampleList
 * @extends {AbstractView}
 */
class SampleList extends AbstractView {
  /**
   * @param {string} id 要素のID
   */
  constructor(id) {
    super(id);

    this._grid = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init(config);

    this._createGrid();
    this._attachEvent();
  }

  /**
   * 更新する
   */
  refresh() {
    const dataModel = this._getDataModel();
    this._grid.setDataModel(dataModel);
    this._grid.refresh();
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.els.newButton.on('click', () => {
      this.trigger('click:new');
    });

    this.els.copyButton.on('click', () => {
      const id = this._getSelectedId();
      this.trigger('click:copy', id);
    });

    this.els.deleteButton.on('click', () => {
      const id = this._getSelectedId();
      this.trigger('click:delete', id);
    });

    this.els.referButton.on('click', () => {
      const id = this._getSelectedId();
      this.trigger('click:refer', id);
    });
  }

  /**
   * グリッドを作成する
   */
  _createGrid() {
    this._grid = new Grid({
      id: config.grid.id,
      colModel: config.grid.colModel,
      dataModel: this._getDataModel()
    });
    this._attachGridEvent();
    this._grid.init();
  }

  /**
   * グリッドに関するイベントを設定する
   */
  _attachGridEvent() {
    const keyList = ['copyButton', 'deleteButton', 'referButton'];

    this._grid.on('refresh', () => {
      this.disable({ els: keyList });
    });

    this._grid.on('select', () => {
      this.enable({ els: keyList });
    });

    this._grid.on('dblclick:row', () => {
      const id = this._getSelectedId();
      this.trigger('click:refer', id);
    });
  }

  /**
   * グリッドのデータモデルを取得する
   * @returns {Array.<Object>}
   */
  _getDataModel() {
    const data = [];
    Model.each((model) => {
      data.push({
        id: model.id,
        fullName: model.get('full_name'),
        jobCategory: model.get('job_category', true)
      });
    });
    return data;
  }

  /**
   * 選択行のIDを取得する
   * @returns {string} ID
   */
  _getSelectedId() {
    const data = this._grid.getSelectedData();
    if (!data || !data.id) return '';
    return data.id;
  }
}

module.exports = SampleList;
