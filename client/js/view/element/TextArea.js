const $ = require('jquery');
const AbstractInput = require('./AbstractInput');

/**
 * textareaタグを作成する
 * @param {string} id id属性
 * @param {number} [width] 幅
 * @returns {Object} $input
 * @returns {Object} $textarea
 */
function makeTextareaTag(id, width) {
  const $textarea = $('<textarea>', {
    id: id
  });
  if (width) $textarea.width(width);
  return $textarea;
}

/**
 * テキストエリア
 * @class TextArea
 * @extends {AbstractInput}
 */
class TextArea extends AbstractInput {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} params.labelName ラベル名
   * @param {number} [params.width] 幅
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.$textarea = this.$el = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // テキストエリア作成
    this.create();
  }

  /**
   * テキストエリアを作成する
   */
  create() {
    this.$textarea = this.$el = makeTextareaTag(this.id, this.width);
    this.$contents.append(this.$textarea);
  }

  /**
   * 値を取得する
   * @returns {string} 要素の値
   */
  get() {
    if (!this.$textarea) return null;
    return this.$textarea.val();
  }

  /**
   * 値を設定する
   * @param {string} val
   */
  set(val) {
    if (!this.$textarea) return;
    this.$textarea.val(val);
  }
}

module.exports = TextArea;
