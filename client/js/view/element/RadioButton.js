const Util = require('../../Util');
const $ = require('jquery');
const Const = require('../../Const');
const AbstractInput = require('./AbstractInput');
const Items = require('./Items');

/**
 * inputタグを作成する
 * @param {string} name name属性
 * @param {string} id id属性
 * @param {string} value value属性
 * @returns {Object} $input
 */
function makeInputTag(name, id, value) {
  const $input = $('<input>', {
    type: 'radio',
    name: name,
    id: id,
    value: value
  });
  return $input;
}

/**
 * labelタグを作成する
 * @param {string} text labelに表示するテキスト
 * @param {string} forId for属性
 * @returns {Object} $label
 */
function makeLabelTag(text, forId) {
  const $label = $('<label></label>', {
    text: text,
    for: forId
  });
  $label.addClass(Const.Class.RADIO_LABEL);
  return $label;
}

/**
 * ラジオボタン
 * @class RadioButton
 * @extends {AbstractInput}
 */
class RadioButton extends AbstractInput {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} params.labelName ラベル名
   * @param {Array.<{name, value}>} params.list ラジオボタンのリスト
   * @param {number} params.colNum 項目の列数
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.list = _params.list || [];
    this.colNum = _params.colNum || null;

    this.$radio = this.$el = null;
    this.name = `${this.id}-radio`;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // ラジオボタン作成
    this.create();
  }

  /**
   * ラジオボタンを作成する
   * @param {Array.<{name, value}>} list ラジオボタンのリスト
   */
  create(list) {
    this.list = list || this.list;

    this.createItems();

    this.$radio = this.$el = this.$contents.find(`input[name=${this.name}]`);
  }

  /**
   * 各項目を作成する
   */
  createItems() {
    this.emptyItems();

    this.list.forEach((item) => {
      const $item = this.makeItem(item.name, item.value);
      this.pushItems($item.input, $item.label);
    });

    this.arrangeItems(this.$contents, this.colNum);
    this.justifyItems();
  }

  /**
   * 項目を作成する
   * @param {string} name
   * @param {string} value
   * @returns {{input: Object, label:Object}}
   */
  makeItem(name, value) {
    const id = `${this.name}-${value}`;
    const $input = makeInputTag(this.name, id, value);
    const $inputLabel = makeLabelTag(name, id);
    return {
      input: $input,
      label: $inputLabel
    };
  }

  /**
   * 値を取得する
   * @returns {string} 要素の値
   */
  get() {
    if (!this.$contents) return null;
    const $checked = this.$contents.find(`input[name=${this.name}]:checked`);
    return $checked.val();
  }

  /**
   * 値を設定する
   * @param {string} val
   */
  set(val) {
    if (!this.$radio) return;
    this.$radio.val([val]);
  }
}

Util.mixin(RadioButton, Items);

module.exports = RadioButton;
