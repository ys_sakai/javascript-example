const $ = require('jquery');
const AbstractElement = require('./AbstractElement');

/**
 * labelタグを作成する
 * @param {Object} params パラメータ
 * @param {string} params.text labelに表示するテキスト
 * @param {string} params.forId for属性に指定するID
 * @param {string} params.className class属性
 * @returns {Object} $label
 */
function makeLabelTag(params) {
  const $label = $('<label></label>', {
    text: params.text
  });
  if (params.forId) $label.attr('for', params.forId);
  if (params.className) $label.addClass(params.className);
  return $label;
}

/**
 * ラベル
 * @class Label
 * @extends {AbstractElement}
 */
class Label extends AbstractElement {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} [params.className] クラス
   * @param {string} params.forId for属性に指定するID
   * @param {string} params.text 要素のテキスト
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.forId = _params.forId;
    this.text = _params.text;

    this.$label = this.$el = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    this.$label = this.$el = makeLabelTag({
      text: this.text,
      forId: this.forId,
      className: this.className
    });
    this.$wrapper.append(this.$label);
  }
}

module.exports = Label;
