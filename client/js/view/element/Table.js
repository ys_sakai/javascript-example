const $ = require('jquery');
const AbstractElement = require('./AbstractElement');

/**
 * tabelタグを作成する
 * @param {string} [className] class属性
 * @returns {Object} $tabel
 */
function makeTableTag(className) {
  const $table = $('<table></table>');
  if (className) $table.addClass(className);
  return $table;
}

/**
 * trタグを作成する
 * @returns {Object} $tr
 */
function makeTrTag() {
  const $tr = $('<tr></tr>');
  return $tr;
}

/**
 * tdタグまたはthタグを作成する
 * @param {string} text セルに表示するテキスト
 * @param {string} colspan colspan属性
 * @param {string} rowspan rowspan属性
 * @param {boolean} [header=false] true: 見出しである、false: 見出しでない
 * @param {number} [width] 幅
 * @returns {Object} $cell
 */
function makeTdOrThTag(text, colspan, rowspan, header, width) {
  const tag = (header) ? '<th></th>' : '<td></td>';
  const $cell = $(tag, {
    text: text
  });
  if (colspan) $cell.attr('colspan', colspan);
  if (rowspan) $cell.attr('rowspan', rowspan);
  if (width) $cell.width(width);
  return $cell;
}

/**
 * テーブル
 * @class Table
 * @extends {AbstractElement}
 */
class Table extends AbstractElement {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} [params.className] クラス
   * @param {Array.<Array.<Object>>} params.cells テーブルのセル
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.cells = _params.cells || [];

    this.$table = this.$el = null;

    this.$cells = [];
    this.$rows = [];
    this.$coordinates = {};
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // テーブル作成
    this.create();
  }

  /**
   * テーブルを作成する
   * @param {Array.<Array.<Object>>} cells テーブルのセル
   */
  create(cells) {
    this.cells = cells || this.cells;
    this.$cells = [];
    this.$rows = [];

    this.$contents.children().remove();

    this.$table = this.$el = makeTableTag(this.className);
    this.$contents.append(this.$table);

    // セル作成
    this._createCells();
    // 座標系作成
    this._createCoord();
  }

  /**
   * テーブルの各セルを作成する
   */
  _createCells() {
    // 行ループ
    this.cells.forEach((row) => {
      const $cols = [];
      const $tr = makeTrTag();
      this.$table.append($tr);

      // 列ループ
      row.forEach((cell) => {
        const $td = makeTdOrThTag(cell.text, cell.colspan, cell.rowspan, cell.header, cell.width);
        $tr.append($td);
        $cols.push($td);
      });

      this.$rows.push($tr);
      this.$cells.push($cols);
    });
  }

  /**
   * テーブルに対する座標系を作成する
   */
  _createCoord() {
    const $coord = this.$coordinates = {};

    this.eachCoord(($cell, x, y) => {
      if (!$coord[x]) $coord[x] = {};
      if (!$coord[x][y]) $coord[x][y] = $cell;
    });
  }

  /**
   * 指定のセルに対するjQueryオブジェクトを取得する。
   * セルは行番号と列番号、またはx座標とy座標によって指定する。
   * @param {Object} params パラメータ
   * @param {string|number} params.row 行番号
   * @param {string|number} params.col 列番号
   * @param {string|number} params.x x座標
   * @param {string|number} params.y y座標
   * @returns {Object} $cell
   */
  getCell(params) {
    const p = this._convertRowColOrXY(params);
    if (p.row && p.col) {
      if (this.hasCell({ row: p.row, col: p.col })) {
        return this.$cells[p.row][p.col];
      }
    }
    if (p.x && p.y) {
      if (this.hasCell({ x: p.x, y: p.y })) {
        return this.$coordinates[p.x][p.y];
      }
    }
    return null;
  }

  /**
   * 指定のセルを保持しているどうか判定する。
   * セルは行番号と列番号、またはx座標とy座標によって指定する。
   * @param {Object} params パラメータ
   * @param {string|number} params.row 行番号
   * @param {string|number} params.col 列番号
   * @param {string|number} params.x x座標
   * @param {string|number} params.y y座標
   * @returns {boolean} true: 保持している、false: 保持していない
   */
  hasCell(params) {
    const p = this._convertRowColOrXY(params);
    if ((!p.row || !p.col) && (!p.x || !p.y)) return false;
    if (p.row && p.col) {
      if (!this.$cells[p.row]) return false;
      if (!this.$cells[p.row][p.col]) return false;
    }
    if (p.x && p.y) {
      if (!this.$coordinates[p.x]) return false;
      if (!this.$coordinates[p.x][p.y]) return false;
    }
    return true;
  }

  /**
   * 各セルのjQueryオブジェクトに対してコールバック関数を実行する
   * @param {Function} callback コールバック関数
   * @param {Object} [thisObj=Table] callback内で使用するthis
   */
  each(callback, thisObj) {
    const _this = thisObj || this;

    this.eachRow(($row, rowIndex, $rows, $cols, $cells) => {
      $cols.forEach(($cell, colIndex) => {
        callback.call(_this, $cell, colIndex, rowIndex, $cells);
      });
    });
  }

  /**
   * 各行のjQueryオブジェクトに対してコールバック関数を実行する
   * @param {Function} callback コールバック関数
   * @param {Object} [thisObj=Table] callback内で使用するthis
   */
  eachRow(callback, thisObj) {
    const _this = thisObj || this;

    this.$rows.forEach(($row, rowIndex, $rows) => {
      callback.call(_this, $row, rowIndex, $rows, this.$cells[rowIndex], this.$cells);
    });
  }

  /**
   * テーブルの各座標に対してコールバック関数を実行する。
   * 座標はテーブルの左上を基準にして、横方向がx、縦方向がy。
   * 結合されたセルは複数座標で成り立つとみなす。
   * @param {Function} callback コールバック関数
   * @param {Object} [thisObj=Table] callback内で使用するthis
   */
  eachCoord(callback, thisObj) {
    const _this = thisObj || this;
    const mergedCells = [];

    // xy座標を表す文字列を取得する
    const getXY = (x, y) => (`x${x}y${y}`);

    // 結合されたセルかどうか判定する
    const isMerged = (x, y) => {
      const xy = getXY(x, y);
      return mergedCells.indexOf(xy) >= 0;
    };

    // 指定のx, yを結合された座標として登録する
    const pushMerged = (x, y) => {
      const xy = getXY(x, y);
      mergedCells.push(xy);
    };

    // 行
    this.eachRow(($row, yIdx, $rows, $cols) => {
      let xIdx = 0;
      // 列
      $cols.forEach(($cell) => {
        while (isMerged(xIdx, yIdx)) xIdx++;

        // セルの左上のxy座標に対するコールバック関数の実行
        callback.call(_this, $cell, xIdx, yIdx);

        const span = this._getSpan($cell);
        for (let rowIndex = 0; rowIndex < span.row; rowIndex++) {
          for (let colIndex = 0; colIndex < span.col; colIndex++) {
            if (rowIndex === 0 && colIndex === 0) continue;

            // 左上のxy座標以外の、結合されたxy座標に対するコールバック関数の実行
            callback.call(_this, $cell, xIdx + colIndex, yIdx + rowIndex);

            pushMerged(xIdx + colIndex, yIdx + rowIndex);
          }
        }
        xIdx++;
      });
    });
  }

  /**
   * 行番号と列番号、またはx座標とy座標を文字列に変換する
   * @param {Object} params パラメータ
   * @param {string|number} params.row 行番号
   * @param {string|number} params.col 列番号
   * @param {string|number} params.x x座標
   * @param {string|number} params.y y座標
   * @returns {{col: (string|null), row: (string|null), x: (string|null), y: (string|null)}}
   */
  _convertRowColOrXY(params) {
    const p = params || {};
    const row = (p.row === undefined) ? null : String(p.row);
    const col = (p.col === undefined) ? null : String(p.col);
    const x = (p.x === undefined) ? null : String(p.x);
    const y = (p.y === undefined) ? null : String(p.y);
    return { row: row, col: col, x: x, y: y };
  }

  /**
   * セルのcolspan属性とrowspan属性を取得する。
   * 属性が設定されていない場合、1となる。
   * @param {Object} $cell セル
   * @returns {{col: number, row: number}}
   */
  _getSpan($cell) {
    const colspan = $cell.attr('colspan');
    const rowspan = $cell.attr('rowspan');
    return {
      col: (!colspan) ? 1 : parseInt(colspan, 10),
      row: (!rowspan) ? 1 : parseInt(rowspan, 10)
    };
  }
}

module.exports = Table;
