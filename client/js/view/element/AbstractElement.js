const $ = require('jquery');
const Const = require('../../Const');
const Util = require('../../Util');

/**
 * 要素の抽象クラス
 * @class AbstractElement
 */
class AbstractElement {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} [params.className] クラス
   * @param {number} [params.width] 幅
   */
  constructor(params) {
    const _params = params || {};

    this.wrapperId = _params.id;
    this.className = _params.className;
    this.width = _params.width || null;

    this.id = Util.generateGuid();

    this.$wrapper = null;
    this.$contents = null;
    this.$el = null;
  }

  /**
   * 初期化する
   */
  init() {
    this.$wrapper = $(`#${this.wrapperId}`);
    this.$contents = this.$wrapper;

    this.$wrapper.children().remove();
  }

  /**
   * 表示する
   */
  show() {
    if (!this.$wrapper) return;
    this.$wrapper.removeClass(Const.Class.DISPLAY_NONE);
    this.$wrapper.removeClass(Const.Class.VISIBILITY_HIDDEN);
  }

  /**
   * 非表示にする
   * @param {Object} [options] オプション
   * @param {boolean} [options.space=false] true: スペースを残す、false: 残さない
   */
  hide(options) {
    options = options || {};
    if (options.space === undefined) options.space = false;
    if (!this.$wrapper) return;

    const className = options.space ?
        Const.Class.VISIBILITY_HIDDEN : Const.Class.DISPLAY_NONE;
    this.$wrapper.addClass(className);
  }

  /**
   * 要素が表示されているか判定する
   * @returns {boolean} true: 表示されている、false: 表示されていない
   */
  isVisible() {
    if (!this.$wrapper) return false;
    if (this.$wrapper.hasClass(Const.Class.DISPLAY_NONE)) return false;
    if (this.$wrapper.hasClass(Const.Class.VISIBILITY_HIDDEN)) return false;
    return true;
  }

  /**
   * イベントにコールバック関数を紐づける
   * @param {string} event イベント名
   * @param {Function} callback コールバック関数
   * @returns {AbstractElement} <code>this</code>
   */
  on(event, callback) {
    if (!this.$el) return this;
    this.$el.on(event, callback);
    return this;
  }

  /**
   * イベントに紐づくコールバック関数を削除する
   * @param {string} event イベント名
   * @param {Function} callback コールバック関数
   * @returns {AbstractElement} <code>this</code>
   */
  off(event, callback) {
    if (!this.$el) return this;
    this.$el.off(event, callback);
    return this;
  }

  /**
   * イベントを発火させ、それに紐づく各コールバック関数を実行する
   * @param {string} event イベント名
   * @param {...*} args コールバック関数に渡す引数（可変長）
   * @returns {AbstractElement} <code>this</code>
   */
  trigger(event, ...args) {
    if (!this.$el) return this;
    this.$el.trigger(event, args);
    return this;
  }
}

module.exports = AbstractElement;
