const $ = require('jquery');
const AbstractElement = require('./AbstractElement');

/**
 * buttonタグを作成する
 * @param {string} text buttonに表示するテキスト
 * @param {number} [width] 幅
 * @returns {Object} $button
 */
function makeButtonTag(text, width) {
  const $button = $('<button></button>', {
    text: text
  });
  if (width) $button.width(width);
  return $button;
}

/**
 * ボタン
 * @class Button
 * @extends {AbstractElement}
 */
class Button extends AbstractElement {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {number} [params.width] 幅
   * @param {string} params.text 要素のテキスト
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.text = _params.text;

    this.$button = this.$el = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    this.$button = this.$el = makeButtonTag(this.text, this.width);
    this.$contents.append(this.$button);
  }

  /**
   * 有効にする
   */
  enable() {
    if (!this.$button) return;
    this.$button.prop('disabled', false);
  }

  /**
   * 無効にする
   */
  disable() {
    if (!this.$button) return;
    this.$button.prop('disabled', true);
  }
}

module.exports = Button;
