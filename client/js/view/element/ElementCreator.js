const Const = require('../../Const');

const TextBox = require('./TextBox');
const RadioButton = require('./RadioButton');
const CheckBox = require('./CheckBox');
const SelectBox = require('./SelectBox');
const TextArea = require('./TextArea');
const Button = require('./Button');
const Table = require('./Table');
const Text = require('./Text');

/**
 * 要素作成
 * @class ElementCreator
 */
class ElementCreator {
  /**
   * 要素を作成する
   * @static
   * @param {Object} configEl 要素の設定
   * @returns {AbstractElement}
   */
  static create(configEl) {
    switch (configEl.type) {
      case Const.ElementType.TEXT_BOX:
        return new TextBox(configEl);
      case Const.ElementType.RADIO_BUTTON:
        return new RadioButton(configEl);
      case Const.ElementType.CHECK_BOX:
        return new CheckBox(configEl);
      case Const.ElementType.SELECT_BOX:
        return new SelectBox(configEl);
      case Const.ElementType.TEXT_AREA:
        return new TextArea(configEl);
      case Const.ElementType.BUTTON:
        return new Button(configEl);
      case Const.ElementType.TABLE:
        return new Table(configEl);
      case Const.ElementType.TEXT:
        return new Text(configEl);
      default:
        throw new Error('指定の要素が存在しません');
    }
  }
}

module.exports = ElementCreator;
