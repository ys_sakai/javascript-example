const $ = require('jquery');
const AbstractInput = require('./AbstractInput');

/**
 * selectタグを作成する
 * @param {string} id id属性
 * @returns {Object} $input
 */
function makeSelectTag(id) {
  const $select = $('<select>', {
    id: id
  });
  return $select;
}

/**
 * optionタグを作成する
 * @param {string} text optionに表示するテキスト
 * @param {string} value value属性
 * @returns {Object} $option
 */
function makeOptionTag(text, value) {
  const $option = $('<option></option>', {
    text: text,
    value: value
  });
  return $option;
}

/**
 * セレクトボックス
 * @class SelectBox
 * @extends {AbstractInput}
 */
class SelectBox extends AbstractInput {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} params.labelName ラベル名
   * @param {Array.<{name, value}>} params.list セレクトボックスのリスト
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.list = _params.list || [];

    this.$selectbox = this.$el = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // セレクトボックス作成
    this.create();
  }

  /**
   * セレクトボックスを作成する
   * @param {Array.<{name, value}>} list セレクトボックスのリスト
   */
  create(list) {
    this.list = list || this.list;

    this.$selectbox = this.$el = makeSelectTag(this.id);
    this.$contents.append(this.$selectbox);

    for (let i = 0, len = this.list.length; i < len; i++) {
      const $option = makeOptionTag(this.list[i].name, this.list[i].value);
      this.$selectbox.append($option);
    }
  }

  /**
   * 値を取得する
   * @returns {string} 要素の値
   */
  get() {
    if (!this.$selectbox) return null;
    return this.$selectbox.val();
  }

  /**
   * 値を設定する
   * @param {string} val
   */
  set(val) {
    if (!this.$selectbox) return;
    this.$selectbox.val(val);
  }
}

module.exports = SelectBox;
