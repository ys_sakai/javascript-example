const $ = require('jquery');
const Const = require('../../Const');
const Util = require('../../Util');
const AbstractElement = require('./AbstractElement');
const Label = require('./Label');

/**
 * タグを作成する
 * @param {Object} params パラメータ
 * @param {string} params.tag htmlタグ
 * @param {string} params.text 表示するテキスト
 * @param {string} params.className class属性
 * @param {string} params.useLabel true: ラベル使用、false: ラベル未使用
 * @returns {Object} $label
 */
function makeTextTag(params) {
  const $text = $(params.tag);
  if (params.text) $text.html(params.text);
  if (params.useLabel) $text.addClass(Const.Class.TEXT_CONTENTS);
  if (params.className) $text.addClass(params.className);
  return $text;
}

/**
 * テキスト
 * @class Text
 * @extends {AbstractElement}
 */
class Text extends AbstractElement {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} [params.className] クラス
   * @param {string} params.text 要素のテキスト
   * @param {string} [params.tag='<div></div>'] 要素のタグ
   * @param {string} [params.labelName] ラベル名
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.text = this.convert(_params.text);
    this.tag = _params.tag || '<div></div>';
    this.labelName = _params.labelName || null;

    this.$text = this.$el = null;
    this.label = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    if (this.labelName) this.createLabel();
    this.createText();
  }

  /**
   * テキストを作成する
   */
  createText() {
    this.$text = this.$el = makeTextTag({
      tag: this.tag,
      text: this.text,
      className: this.className,
      useLabel: (!!this.labelName)
    });
    this.$wrapper.append(this.$text);
  }

  /**
   * ラベルを作成する
   */
  createLabel() {
    this.label = new Label({
      id: this.wrapperId,
      forId: this.id,
      text: `${this.labelName}：`,
      className: Const.Class.TEXT_LABEL
    });
    this.label.init();
  }

  /**
   * テキストを設定する
   * @param {string} text テキスト
   */
  set(text) {
    if (!this.$text) return;
    this.text = this.convert(text);
    this.$text.html(this.text);
  }

  /**
   * テキストを出力用に変換する
   * @param {string} text テキスト
   * @returns {string}
   */
  convert(text) {
    text = Util.escapeHtml(text);
    text = this._convertLinefeedCode(text);
    return text;
  }

  /**
   * テキスト内の改行コードを<br>タグに変換する
   * @param {stirng} text テキスト
   * @returns {string}
   */
  _convertLinefeedCode(text) {
    text = Util.unityLinefeedCode(text);
    return text.replace(/\n/g, '<br />');
  }
}

module.exports = Text;
