const $ = require('jquery');
const AbstractInput = require('./AbstractInput');

/**
 * inputタグを作成する
 * @param {string} id id属性
 * @param {number} [width] 幅
 * @returns {Object} $input
 */
function makeInputTag(id, width) {
  const $input = $('<input>', {
    type: 'text',
    id: id
  });
  if (width) $input.width(width);
  return $input;
}

/**
 * テキストボックス
 * @class TextBox
 * @extends {AbstractInput}
 */
class TextBox extends AbstractInput {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} params.labelName ラベル名
   * @param {number} [params.width] 幅
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.$textbox = this.$el = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // テキストボックス作成
    this.create();
  }

  /**
   * テキストボックスを作成する
   */
  create() {
    this.$textbox = this.$el = makeInputTag(this.id, this.width);
    this.$contents.append(this.$textbox);
  }

  /**
   * 値を取得する
   * @returns {string} 要素の値
   */
  get() {
    if (!this.$textbox) return null;
    return this.$textbox.val();
  }

  /**
   * 値を設定する
   * @param {string} val
   */
  set(val) {
    if (!this.$textbox) return;
    this.$textbox.val(val);
  }
}

module.exports = TextBox;
