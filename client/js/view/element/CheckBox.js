const Util = require('../../Util');
const $ = require('jquery');
const Const = require('../../Const');
const AbstractInput = require('./AbstractInput');
const Items = require('./Items');

/**
 * inputタグを作成する
 * @param {string} name name属性
 * @param {string} id id属性
 * @param {string} value value属性
 * @returns {Object} $input
 */
function makeInputTag(name, id, value) {
  const $input = $('<input>', {
    type: 'checkbox',
    name: name,
    id: id,
    value: value
  });
  return $input;
}

/**
 * labelタグを作成する
 * @param {string} text labelに表示するテキスト
 * @param {string} forId for属性
 * @returns {Object} $label
 */
function makeLabelTag(text, forId) {
  const $label = $('<label></label>', {
    text: text,
    for: forId
  });
  $label.addClass(Const.Class.CHECKBOX_LABEL);
  return $label;
}

/**
 * チェックボックス
 * @class CheckBox
 * @extends {AbstractInput}
 */
class CheckBox extends AbstractInput {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} params.labelName ラベル名
   * @param {Array.<{name, value}>} params.list チェックボックスのリスト
   * @param {number} params.colNum 項目の列数
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.list = _params.list || [];
    this.colNum = _params.colNum || null;

    this.$checkbox = this.$el = null;
    this.name = `${this.id}-checkbox`;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // チェックボックス作成
    this.create();
  }

  /**
   * チェックボックスを作成する
   * @param {Array.<{name, value}>} list チェックボックスのリスト
   */
  create(list) {
    this.list = list || this.list;

    this.createItems();

    this.$checkbox = this.$el = this.$contents.find(`input[name=${this.name}]`);
  }

/**
 * 各項目を作成する
 */
  createItems() {
    this.emptyItems();

    this.list.forEach((item) => {
      const $item = this.makeItem(item.name, item.value);
      this.pushItems($item.input, $item.label);
    });

    this.arrangeItems(this.$contents, this.colNum);
    this.justifyItems();
  }

  /**
   * 項目を作成する
   * @param {string} name
   * @param {string} value
   * @returns {{input: Object, label:Object}}
   */
  makeItem(name, value) {
    const id = `${this.name}-${value}`;
    const $input = makeInputTag(this.name, id, value);
    const $inputLabel = makeLabelTag(name, id);
    return {
      input: $input,
      label: $inputLabel
    };
  }

  /**
   * 値を取得する
   * @returns {Array.<string>} 要素の値
   */
  get() {
    if (!this.$contents) return null;

    const values = [];
    const $checked = this.$contents.find(`input[name=${this.name}]:checked`);

    $checked.each(function callback() {
      values.push($(this).val());
    });
    return values;
  }

  /**
   * 値を設定する
   * @param {Array.<string>} vals
   */
  set(vals) {
    if (!this.$checkbox) return;
    this.$checkbox.val(vals);
  }
}

Util.mixin(CheckBox, Items);

module.exports = CheckBox;
