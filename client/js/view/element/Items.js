const $ = require('jquery');
const Const = require('../../Const');

/**
 * spanタグを作成する
 * @returns {Object} $span
 */
function makeSpanTag() {
  const $span = $('<span></span>');
  $span.addClass(Const.Class.INPUT_ITEM);
  return $span;
}

/**
 * divタグを作成する
 * @returns {Object} $div
 */
function makeDivTag() {
  const $div = $('<div></div>');
  return $div;
}

/**
 * 項目
 * @class Items
 */
class Items {
  /**
   * 項目の配列を空にする
   */
  emptyItems() {
    this.$items = [];
  }

  /**
   * 項目を配列に追加する
   * @param {...*} $els 項目の要素（可変長）
   */
  pushItems(...$els) {
    if (!this.$items) this.emptyItems();

    const $item = makeSpanTag();
    $els.forEach(($el) => {
      $item.append($el);
    });
    this.$items.push($item);
  }

  /**
   * 各項目を配置する
   * @param {Object} $contents コンテンツ
   * @param {number} colNum 項目の列数
   */
  arrangeItems($contents, colNum) {
    const _colNum = colNum || this.$items.length;
    const skips = [];

    // スキップするインデックスかどうか判定する
    const isSkipped = (i) => skips.indexOf(i) >= 0;

    this.eachItems(($item, index, $items) => {
      if (isSkipped(index)) return;
      // 行
      let count = 0;
      const $row = makeDivTag();
      $contents.append($row);

      // 列
      while (count < _colNum) {
        const i = index + count;
        $row.append($items[i]);
        skips.push(i);
        count++;
      }
    });
  }

  /**
   * 各項目の幅を揃える
   */
  justifyItems() {
    let max = 0;
    this.eachItems(($item) => {
      const width = $item.width();
      max = (max > width) ? max : width;
    });
    this.eachItems(($item) => {
      $item.width(max);
    });
  }

  /**
   * 各項目に対してコールバック関数を実行する
   * @param {Function} callback コールバック関数
   * @param {Object}  [thisObj=Items] callback内で使用するthis
   */
  eachItems(callback, thisObj) {
    const _this = thisObj || this;
    if (!this.$items) this.emptyItems();

    this.$items.forEach(($item, index, $items) => {
      callback.call(_this, $item, index, $items);
    });
  }
}

module.exports = Items;
