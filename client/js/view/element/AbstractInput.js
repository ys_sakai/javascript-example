const $ = require('jquery');
const Const = require('../../Const');
const AbstractElement = require('./AbstractElement');
const Label = require('./Label');

/**
 * divタグを作成する
 * @returns {Object} $div
 */
function makeDivTag() {
  const $div = $('<div></div>');
  $div.addClass(Const.Class.INPUT_CONTENTS);
  return $div;
}

/**
 * インプット要素の抽象クラス
 * @class AbstractInput
 * @extends {AbstractElement}
 */
class AbstractInput extends AbstractElement {
  /**
   * @param {Object} params パラメータ
   * @param {string} params.id 要素のID
   * @param {string} params.labelName ラベル名
   * @param {string} [params.className] クラス
   * @param {number} [params.width] 幅
   */
  constructor(params) {
    const _params = params || {};

    super(_params);

    this.labelName = _params.labelName;

    this.label = null;
  }

  /**
   * 初期化する
   */
  init() {
    super.init();

    // ラベル作成
    this.createLabel();
    // コンテンツ作成
    this.createContents();
  }

  /**
   * 値を取得する
   */
  get() {
    throw new Error('abstract method');
  }

  /**
   * 値を設定する
   */
  set() {
    throw new Error('abstract method');
  }

  /**
   * 有効にする
   */
  enable() {
    if (!this.$el) return;
    this.$el.prop('disabled', false);
  }

  /**
   * 無効にする
   */
  disable() {
    if (!this.$el) return;
    this.$el.prop('disabled', true);
  }

  /**
   * ラベルを作成する
   */
  createLabel() {
    this.label = new Label({
      id: this.wrapperId,
      forId: this.id,
      text: `${this.labelName}：`,
      className: Const.Class.INPUT_LABEL
    });
    this.label.init();
  }

  /**
   * コンテンツを作成する
   */
  createContents() {
    this.$contents = makeDivTag();
    this.$wrapper.append(this.$contents);
  }
}

module.exports = AbstractInput;
