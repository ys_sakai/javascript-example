const Const = require('../Const');
const AbstractView = require('./AbstractView');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * 要素の設定
   * @type {Object}
   */
  els: {
    message: {
      id: 'error-message',
      type: Const.ElementType.TEXT
    },
    reloadButton: {
      id: 'error-reload-button',
      type: Const.ElementType.BUTTON,
      text: '再読み込み',
      width: 90
    }
  }
};

/**
 * エラーページ ビュー
 * @class ErrorPage
 * @extends {AbstractView}
 */
class ErrorPage extends AbstractView {
  /**
   * 初期化する
   */
  init() {
    super.init(config);

    this._attachEvent();
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.els.reloadButton.on('click', () => this.trigger('reload'));
  }

  /**
   * メッセージを設定する
   * @param {Error} error エラー
   */
  set(error) {
    const message = this._getMessage(error);
    super.set({
      message: message
    });
  }

  /**
   * エラーメッセージを取得する
   * @param {Error} error エラー
   * @returns {string}
   */
  _getMessage(error) {
    const detail = this._getDetailMessage(error);
    let msg = 'エラーが発生しました';
    if (error.message) {
      msg += '：\n\n';
      msg += error.message;
    }
    if (detail !== '') {
      msg += '\n';
      msg += detail;
    }
    return msg;
  }

  /**
   * 詳細なエラーメッセージを取得する
   * @param {Error} error エラー
   * @returns {string}
   */
  _getDetailMessage(error) {
    if (!error.data) return '';
    return JSON.stringify(error.data);
  }
}

module.exports = ErrorPage;
