const Const = require('../Const');
const Model = require('../model/Sample');
const AbstractView = require('./AbstractView');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * 要素の設定
   * @type {Object}
   */
  els: {
    firstName: {
      id: 'form-firstName',
      type: Const.ElementType.TEXT_BOX,
      labelName: '姓',
      width: 100
    },
    lastName: {
      id: 'form-lastName',
      type: Const.ElementType.TEXT_BOX,
      labelName: '名',
      width: 100
    },
    sex: {
      id: 'form-sex',
      type: Const.ElementType.RADIO_BUTTON,
      labelName: '性別',
    },
    jobCategory: {
      id: 'form-jobCategory',
      type: Const.ElementType.SELECT_BOX,
      labelName: '職種'
    },
    certificate: {
      id: 'form-certificate',
      type: Const.ElementType.CHECK_BOX,
      labelName: '資格',
      colNum: 2
    },
    remarks: {
      id: 'form-remarks',
      type: Const.ElementType.TEXT_AREA,
      labelName: '備考',
      width: 300
    },
    saveButton: {
      id: 'form-save-button',
      type: Const.ElementType.BUTTON,
      text: '保　存',
      width: 90
    },
    cancelButton: {
      id: 'form-cancel-button',
      type: Const.ElementType.BUTTON,
      text: 'キャンセル',
      width: 90
    }
  },
  /**
   * リストを設定する要素
   * @type {Object}
   */
  lists: {
    sex: 'sex',
    jobCategory: 'job_category',
    certificate: 'certificate'
  }
};

/**
 * サンプルフォーム ビュー
 * @class SampleForm
 * @extends {AbstractView}
 */
class SampleForm extends AbstractView {
  /**
   * 初期化する
   */
  init() {
    this._updateConfig();
    super.init(config);

    this._attachEvent();
  }

  /**
   * 更新する
   */
  refresh() {
    this._updateConfig();
    const configEls = this._getUpdatedConfig();
    super.create(configEls);
  }

  /**
   * データを設定する
   * @param {Model} model モデルのインスタンス
   */
  set(model) {
    const data = this._getModelData(model);
    super.set(data);
  }

  /**
   * データを取得する
   * @returns {Object}
   */
  get() {
    return this._getViewData();
  }

  /**
   * 設定を更新する
   */
  _updateConfig() {
    const keys = Object.keys(config.lists);
    keys.forEach((key) => {
      config.els[key].list = Model.getList(config.lists[key]);
    });
  }

  /**
   * 更新された設定を取得する
   * @returns {Object}
   */
  _getUpdatedConfig() {
    const configEls = {};
    const keys = Object.keys(config.lists);
    keys.forEach((key) => {
      configEls[key] = config.els[key];
    });
    return configEls;
  }

  /**
   * イベントを設定する
   */
  _attachEvent() {
    this.els.saveButton.on('click', () => this.trigger('save'));
    this.els.cancelButton.on('click', () => this.trigger('cancel'));
  }

  /**
   * ビューに表示するモデルのデータを取得する
   * @param {Model} model モデルのインスタンス
   * @returns {Object} データ
   */
  _getModelData(model) {
    return {
      firstName: model.get('first_name'),
      lastName: model.get('last_name'),
      sex: model.get('sex'),
      jobCategory: model.get('job_category'),
      certificate: model.get('certificate'),
      remarks: model.get('remarks')
    };
  }

  /**
   * モデルに設定するビューの各要素の値を取得する
   * @returns {Object}
   */
  _getViewData() {
    const data = super.get();
    return {
      first_name: data.firstName,
      last_name: data.lastName,
      sex: data.sex,
      job_category: data.jobCategory,
      certificate: data.certificate,
      remarks: data.remarks
    };
  }
}

module.exports = SampleForm;
