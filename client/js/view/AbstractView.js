const Util = require('../Util');
const $ = require('jquery');
const Const = require('../Const');
const Event = require('../Event');
const ElementCreator = require('./element/ElementCreator');

/**
 * 抽象ビュー
 * @class AbstractView
 */
class AbstractView {
  /**
   * @param {string} id 要素のID
   */
  constructor(id) {
    this.id = id;

    this.$view = null;
    this.els = {};
  }

  /**
   * 初期化する
   * @param {Object} config ビューの設定
   * @param {Object} config.els 各要素の設定
   */
  init(config) {
    this.$view = $(`#${this.id}`);

    // ビュー作成
    this.create(config.els);
  }

  /**
   * ビューを作成する
   * @param {Object} configEls 各要素の設定
   */
  create(configEls) {
    const keys = Object.keys(configEls);

    keys.forEach((key) => {
      this.els[key] = ElementCreator.create(configEls[key]);
      if (this.els[key].init) this.els[key].init();
    });
  }

  /**
   * 各要素の値を取得する
   * @returns {Object}
   */
  get() {
    const obj = {};
    this.each((el, key) => {
      if (el.get) obj[key] = el.get();
    });
    return obj;
  }

  /**
   * 各要素に値を設定する
   * @param {Object} data データ
   */
  set(data) {
    const keys = Object.keys(data);

    keys.forEach((key) => {
      if (!this.has(key)) return;
      if (!this.els[key].set) return;
      this.els[key].set(data[key]);
    });
  }

  /**
   * 指定のキーに紐づく要素を保持しているか判定する
   * @param {string} key キー
   * @returns {boolean} true: 保持している、false: 保持していない
   */
  has(key) {
    return (this.els[key] !== undefined);
  }

  /**
   * 各要素を表示する
   * @param {Object} [options] オプション
   * @param {Object} [options.els] 対象の要素（省略時: 全要素）
   */
  show(options) {
    options = options || {};

    this.eachKey(options.els, (el) => {
      if (el.show) el.show();
    });

    this.$view.removeClass(Const.Class.DISPLAY_NONE);
    this.$view.removeClass(Const.Class.VISIBILITY_HIDDEN);
  }

  /**
   * 各要素を非表示にする
   * @param {Object} [options] オプション
   * @param{Object} [options.els] 対象の要素（省略時: 全要素）
   * @param {boolean} [options.space=false] true: スペースを残す、false: 残さない
   */
  hide(options) {
    options = options || {};
    if (options.space === undefined) options.space = false;

    this.eachKey(options.els, (el) => {
      if (el.hide) el.hide(options);
    });

    if (!options.els) {
      const className = options.space ?
          Const.Class.VISIBILITY_HIDDEN : Const.Class.DISPLAY_NONE;
      this.$view.addClass(className);
    }
  }

  /**
   * 各要素を有効にする
   * @param {Object} [options] オプション
   * @param {Object} [options.els] 対象の要素（省略時: 全要素）
   */
  enable(options) {
    options = options || {};
    this.eachKey(options.els, (el) => {
      if (el.enable) el.enable();
    });
  }

  /**
   * 各要素を無効にする
   * @param {Object} [options] オプション
   * @param {Object} [options.els] 対象の要素（省略時: 全要素）
   */
  disable(options) {
    options = options || {};
    this.eachKey(options.els, (el) => {
      if (el.disable) el.disable();
    });
  }

  /**
   * 各要素に対してコールバック関数を実行する
   * @param {Function} callback コールバック関数
   * @param {Object}  [thisObj=AbstractView] callback内で使用するthis
   */
  each(callback, thisObj) {
    this.eachKey(null, callback, thisObj);
  }

  /**
   * 指定のキーに紐づく各要素に対してコールバック関数を実行する
   * @param {Array.<string>} keys キーの配列
   * @param {Function} callback コールバック関数
   * @param {Object}  [thisObj=AbstractView] callback内で使用するthis
   */
  eachKey(keys, callback, thisObj) {
    const _this = thisObj || this;
    const _keys = keys || Object.keys(this.els);

    _keys.forEach((key) => {
      callback.call(_this, this.els[key], key, this.els);
    });
  }
}

Util.mixin(AbstractView, Event);

module.exports = AbstractView;
