/**
 *  ビットマスク
 * @class Bitmask
 */
class Bitmask {
  /**
   * @param {number} [mask] ビットマスク
   */
  constructor(mask) {
    this.mask = parseInt(mask, 10) || 0;
  }

  /**
   * フラグ番号の配列からビットマスクを生成する
   * @static
   * @param {Array.<numeric>} flagNos ONにするフラグ番号の配列
   * @returns {Bitmask}
   */
  static createFromFlagNos(flagNos) {
    const bitmask = new this();
    bitmask.add(flagNos);
    return bitmask;
  }

  /**
   * フラグ番号をビットマスクに変換する
   * @static
   * @param {number} flagNo フラグ番号
   * @returns {number}
   */
  static translate(flagNo) {
    if (flagNo < 0 || flagNo > 31) throw new Error('Bitmask - out of range');
    return 1 << flagNo;
  }

  /**
   * 指定のフラグ番号をONにする
   * @param {number|Array.<number>} flagNo フラグ番号
   */
  add(flagNo) {
    if (Array.isArray(flagNo)) {
      flagNo.forEach((value) => this.add(value));
    } else {
      this.mask |= Bitmask.translate(flagNo);
    }
  }

  /**
   * 指定のフラグ番号をOFFにする
   * @param {number|Array.<number>} flagNo フラグ番号
   */
  remove(flagNo) {
    if (Array.isArray(flagNo)) {
      flagNo.forEach((value) => this.remove(value));
    } else {
      this.mask &= ~Bitmask.translate(flagNo);
    }
  }

  /**
   * 指定のフラグ番号のON／OFFを反転する
   * @param {number|Array.<number>} flagNo フラグ番号
   */
  toggle(flagNo) {
    if (Array.isArray(flagNo)) {
      flagNo.forEach((value) => this.toggle(value));
    } else {
      this.mask ^= Bitmask.translate(flagNo);
    }
  }

  /**
   * 指定のフラグ番号がONであるか判定する
   * @param {number} flagNo フラグ番号
   * @returns {boolean} true: ON、false: OFF
   */
  test(flagNo) {
    const mask = Bitmask.translate(flagNo);
    return (this.mask & mask) === mask;
  }

  /**
   * プリミティブな値を返却する
   * @returns {number}
   */
  valueOf() {
    return this.mask;
  }

  /**
   * JSON形式に変換する
   * @returns {number}
   */
  toJSON() {
    return this.valueOf();
  }
}

module.exports = Bitmask;
