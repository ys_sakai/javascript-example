const Model = require('./Model');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * リソースのURL
   * @type {string}
   */
  url: '/api/master_codes/',
  /**
   * デフォルト値
   * @type {Object}
   */
  defaults: {
    group_code: '',  // グループCD
    code: '', // コード
    group_name: '',  // グループ名称
    name: ''  // 名称
  }
};

/**
 * コードマスタ
 * @class CodeMaster
 * @extends {Model}
 */
class CodeMaster extends Model {
  /**
   * 引数をキーとしてマスタを検索する。
   * 第二引数省略時は第一引数のみがキーとなる。
   * 第一、第二引数省略時は全件検索となる。
   * @static
   * @param {string} [groupCode] グループCD
   * @param {string} [code] コード
   * @returns {Array.<Model>} モデルの配列
   */
  static find(groupCode, code) {
    const predicate = (model) => {
      const key1 = model.get('group_code');
      const key2 = model.get('code');

      if (groupCode && code) return (key1 === groupCode && key2 === code);
      if (groupCode) return (key1 === groupCode);
      return true;
    };
    return super.find(predicate);
  }

  /**
   * グループCDによりリストを取得する
   * @static
   * @param {string} groupCode グループCD
   * @returns {Array.<{name, value}>} リスト
   */
  static getList(groupCode) {
    const list = [];
    const models = this.find(groupCode);
    models.forEach((model) => {
      list.push({
        name: model.get('name'),
        value: model.get('code')
      });
    });
    return list;
  }

  /**
   * グループCD、コードにより名称を取得する
   * @static
   * @param {string} groupCode グループCD
   * @param {string} code コード
   * @returns {string} 名称
   */
  static getName(groupCode, code) {
    const models = this.find(groupCode, code);
    const model = models.shift();
    if (!model) return '';
    return model.get('name');
  }
}

CodeMaster.init({
  url: config.url,
  defaults: config.defaults
});

module.exports = CodeMaster;
