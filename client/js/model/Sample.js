const Model = require('./Model');
const CodeMaster = require('./CodeMaster');
const Bitmask = require('../Bitmask');

/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * リソースのURL
   * @type {string}
   */
  url: '/api/samples/',
  /**
   * デフォルト値
   * @type {Object}
   */
  defaults: {
    first_name: '',  // 姓
    last_name: '', // 名
    sex: '01',  // 性別
    job_category: '01',  // 職種
    certificate: [],  // 資格
    remarks: '' // 備考
  },
  /**
   * コードマスタのマップ
   * @type {Object}
   */
  masterMap: {
    sex: '001',
    job_category: '002',
    certificate: '003'
  },
  /**
   * ビットマスク項目
   * @type {Object}
   */
  bitmasks: ['certificate']
};

/**
 * サンプル
 * @class Sample
 * @extends {Model}
 */
class Sample extends Model {
  /**
   * キーに紐づくリストをコードマスタから取得する
   * @static
   * @param {string} key キー
   * @returns {Array.<{name, value}>} リスト
   */
  static getList(key) {
    const groupCode = config.masterMap[key];
    if (!groupCode) return [];
    return CodeMaster.getList(groupCode);
  }

  /**
   * キーに紐づく値を取得する。
   * isNameをtrueにすると、値を名称に変換して返却する。
   * @static
   * @param {string} key キー
   * @param {Boolean} [isName=false] 名称に変換するかどうか
   * @returns {*} 値
   */
  get(key, isName) {
    if (key === 'full_name') return this.getFullName();
    if (this.isBitmask(key) && !isName) return this.getFromBitmask(key);
    if (isName) return this.getName(key);
    return super.get(key);
  }

  /**
   * キーに紐づく名称をコードマスタから取得する
   * @static
   * @param {string} key キー
   * @returns {stirng} 名称
   */
  getName(key) {
    const groupCode = config.masterMap[key];
    const code = this.get(key);

    if (!groupCode || !code) return '';
    if (Array.isArray(code)) return this._getNames(groupCode, code);
    return CodeMaster.getName(groupCode, code);
  }

  /**
   * グループCDとコードの配列から名称を取得する
   * @static
   * @param {string} groupCode グループCD
   * @param {Array.<string>} codes コードの配列
   * @returns {string}
   */
  _getNames(groupCode, codes) {
    let str = '';
    codes.forEach((code) => {
      const name = CodeMaster.getName(groupCode, code);
      str += (str === '') ? name : `\n${name}`;
    });
    return str;
  }

  /**
   * 氏名を取得する
   * @static
   * @returns {string}
   */
  getFullName() {
    const firstName = this.get('first_name');
    const lastName = this.get('last_name');
    return `${firstName}  ${lastName}`;
  }

  /**
   * ビットマスク項目からコードの配列を取得する
   * @param {string} key キー
   * @returns {Array.<string>}
   */
  getFromBitmask(key) {
    const mask = super.get(key);
    const bitmask = new Bitmask(mask);

    const list = Sample.getList(key);
    const codes = list.map((obj) => obj.value);

    return codes.filter((value) => bitmask.test(value));
  }

  /**
   * データ設定時に、キーに紐づく値を変換する
   * @param {string} key キー
   * @param {*} value 値
   * @returns {*}
   */
  parse(key, value) {
    if (this.isBitmask(key)) return this.parseBitmask(value);
    return super.parse(key, value);
  }

  /**
   * 値をビットマスク値に変換する
   * @param {Array.<string>|number} value 値
   * @returns {number}
   */
  parseBitmask(value) {
    if (Array.isArray(value)) {
      return Bitmask.createFromFlagNos(value).valueOf();
    } else {
      return value;
    }
  }

  /**
   * キーがビットマスク項目であるか判定する
   * @param {string} key キー
   * @returns {boolean}
   */
  isBitmask(key) {
    return config.bitmasks.indexOf(key) >= 0;
  }
}

Sample.init({
  url: config.url,
  defaults: config.defaults
});

module.exports = Sample;
