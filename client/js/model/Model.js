const Util = require('../Util');
const Event = require('../Event');
const api = require('../api');

/**
 * クライアントID識別子
 * @type {string}
 */
const CID = 'cid';

/**
 * モデル
 * @class Model
 */
class Model {
  /**
   * @param {Object} data データ（key-valueの連想配列）
   */
  constructor(data) {
    this.data = {};
    if (this.defaults) this.set(this.defaults, true);
    if (data) this.set(data, true);

    this.data.id = this.data.id || this.getNextClientId();
  }

  /**
   * インスタンス一覧
   * @readonly
   * @static
   * @type {Map.<string, Model>}
   */
  static get models() {
    this._models = this._models || new Map();
    return this._models;
  }

  /**
   * リソースのURL
   * @readonly
   * @static
   * @type {string}
   */
  static get url() {
    return this.prototype._url;
  }

  /**
   * 初期化する
   * @static
   * @param {Object} params
   * @param {Object} params.url リソースのURL
   * @param {Object} [params.defaults] モデルに設定するデフォルトのデータ
   */
  static init(params) {
    const _params = params || {};

    this.prototype._url = _params.url;
    this.prototype.defaults = _params.defaults || null;

    this.models.clear();
  }

  /**
   * インスタンスを新規作成する
   * @static
   * @param {Object} data データ（key-valueの連想配列）
   * @returns {Model} インスタンス
   */
  static create(data) {
    return new this(data);
  }

  /**
   * 指定のIDに紐づくインスタンスを複製する
   * @static
   * @param {string} id ID
   * @returns {Model} インスタンス
   */
  static copy(id) {
    const model = this.models.get(id);
    const clone = model.clone();
    clone.data.id = clone.getNextClientId();
    return clone;
  }

  /**
   * 一覧にインスタンスを追加する。
   * 同一IDのインスタンスが既に一覧に存在する場合、上書きされる。
   * @static
   * @param {Model} model インスタンス
   */
  static add(model) {
    this.models.set(model.id, model);
    this.trigger('change', this);
  }

  /**
   * 一覧からインスタンスを削除する
   * @static
   * @param {Model} model インスタンス
   */
  static remove(model) {
    this.models.delete(model.id);
    this.trigger('change', this);
  }

  /**
   * 各インスタンスを読み込む
   * @static
   * @returns {Promise.<Function|Error>}
   */
  static fetch() {
    this.models.clear();

    return api.sync('read', this)
      .then((records) => {
        records.forEach((record) => {
          const model = new this(record);
          this.add(model);
        });
        return this;
      });
  }

  /**
   * 各インスタンスに対して条件判定を行い、判定がtrueのインスタンスの複製の配列を返却する
   * @static
   * @param {Function} predicate 条件判定用の関数
   * @returns {Array.<Model>} インスタンスの配列
   */
  static find(predicate) {
    const results = [];
    this.each((model, id, models) => {
      if (predicate(model, id, models)) {
        results.push(model.clone());
      }
    });
    return results;
  }

  /**
   * 指定のIDに紐づくインスタンスの複製を取得する
   * @static
   * @param {string} id ID
   * @returns {Model}
   */
  static findById(id) {
    const model = this.models.get(id);
    if (!model) throw new Error('指定のインスタンスが見つかりません');
    return model.clone();
  }

  /**
   * 指定のIDに紐づくインスタンスが存在するか判定する
   * @static
   * @param {string} id ID
   * @returns {boolean} true: 存在する、false: 存在しない
   */
  static exists(id) {
    return this.models.has(id);
  }

  /**
   * 各インスタンスに対してコールバック関数を実行する
   * @static
   * @param {Function} callback コールバック
   */
  static each(callback) {
    this.models.forEach(callback);
  }

  /**
   * リソースのURL
   * @readonly
   * @type {string}
   */
  get url() {
    if (this.isNew()) return this._url;
    return this._url.replace(/[^\/]$/, '$&/') + this.id;
  }

  /**
   * ID
   * @readonly
   * @type {string}
   */
  get id() {
    return this.data.id;
  }

  /**
   * キーに紐づく値を取得する
   * @param {string} key キー
   * @returns {*} 値
   */
  get(key) {
    return this.data[key];
  }

  /**
   * データを設定する
   * @param {Object} data データ
   * @param {boolean} [isSilent=false] true: 変更イベントを発火させない
   */
  set(data, isSilent) {
    const changes = [];
    const keys = Object.keys(data);

    keys.forEach((key) => {
      const value = this.parse(key, data[key]);
      if (this.data[key] === value) return;
      this.data[key] = value;
      changes.push(key);
    });

    if (!isSilent) {
      changes.forEach((key) => {
        this.trigger(`change:${key}`, this, this.data[key]);
      });
    }

    if (!isSilent && changes.length !== 0) {
      this.trigger('change', this);
    }
  }

  /**
   * データ設定時に、指定のキーに紐づく値を変換する
   * @param {string} key キー
   * @param {*} value 値
   * @returns {*}
   */
  parse(key, value) {
    return value;
  }

  /**
   * 指定のキーを保持しているか判定する
   * @param {string} key キー
   * @returns {boolean} true: 保持している、false: 保持していない
   */
  has(key) {
    return (this.data[key] !== undefined);
  }

  /**
   * 新規インスタンスか判定する。
   * IDがクライアントIDの場合に新規インスタンスとみなす。
   * @returns {boolean} true: 新規、false: 登録済み
   */
  isNew() {
    const regex = new RegExp(`^${CID}`);
    return regex.test(this.id);
  }

  /**
   * 読み込む
   * @returns {Promise.<Model|Error>}
   */
  fetch() {
    return api.sync('read', this)
      .then((record) => {
        this.set(record);
        return this;
      });
  }

  /**
   * 保存する
   * @returns {Promise.<Model|Error>}
   */
  save() {
    const method = this.isNew() ? 'create' : 'update';
    return api.sync(method, this)
      .then((record) => {
        this.set(record);
        this.constructor.add(this);
        return this;
      });
  }

  /**
   * 破棄する
   * @returns {Promise.<Model|Error>}
   */
  destroy() {
    return api.sync('delete', this)
      .then(() => {
        this.constructor.remove(this);
        return this;
      });
  }

  /**
   * 自身を複製する
   * @returns {Model} 複製したインスタンス
   */
  clone() {
    const data = Util.copy(this.data);
    return new this.constructor(data);
  }

  /**
   * クライアントIDを採番する
   * @returns {string} ID
   */
  getNextClientId() {
    return `${CID}_${Util.generateGuid()}`;
  }

  /**
   * JSON形式に変換する
   * @returns {Object}
   */
  toJSON() {
    return this.data;
  }

  /**
   * イベント
   * @readonly
   * @static
   * @type {Event}
   */
  static get event() {
    this._event = this._event || new Event();
    return this._event;
  }

  // Eventの各メソッドをstaticで使用可能にする
  static on(event, callback) { return this.event.on(event, callback); }
  static off(event, callback) { return this.event.off(event, callback); }
  static trigger(event, ...args) { return this.event.trigger(event, ...args); }
}

Util.mixin(Model, Event);

module.exports = Model;
