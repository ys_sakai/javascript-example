/**
 * 定数定義
 * @type {Object}
 */
const Const = {
  /**
   * 要素タイプ
   * @type {Object}
   */
  ElementType: {
    TEXT_BOX: '1',             // TextBox
    RADIO_BUTTON: '2',   // RadioButton
    CHECK_BOX: '3',          // CheckBox
    SELECT_BOX: '4',         // SelectBox
    TEXT_AREA: '5',           // TextArea
    BUTTON: '6',               // Button
    TABLE: '7',                   // Table
    TEXT: '8'                      // Text
  },
  /**
   * クラス
   * @type {Object}
   */
  Class: {
    INPUT_LABEL: 'app-input-label',
    INPUT_CONTENTS: 'app-input-contents',
    INPUT_ITEM: 'app-input-item',

    RADIO_LABEL: 'app-radio-label',
    CHECKBOX_LABEL: 'app-checkbox-label',

    TEXT_LABEL: 'app-text-label',
    TEXT_CONTENTS: 'app-text-contents',

    VISIBILITY_HIDDEN: 'app-visibility-hidden',
    DISPLAY_NONE: 'app-display-none',

    GRID: 'app-grid',
    GRID_IS_HOVER: 'app-grid-is-hover',
    GRID_IS_SELECTED: 'app-grid-is-selected'
  }
};

module.exports = Const;
