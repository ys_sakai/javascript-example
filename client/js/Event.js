/**
 * イベント
 * @class Event
 */
class Event {
  /**
   * イベントにコールバック関数を紐づける
   * @param {string} event イベント名
   * @param {Function} callback コールバック関数
   * @returns {Event} <code>this</code>
   */
  on(event, callback) {
    if (!this._callbacks) this._callbacks = {};
    if (!this._callbacks[event]) this._callbacks[event] = [];
    this._callbacks[event].push(callback);
    return this;
  }

  /**
   * イベントに紐づくコールバック関数を削除する
   * @param {string} event イベント名
   * @param {Function} callback コールバック関数
   * @returns {Event} <code>this</code>
   */
  off(event, callback) {
    if (!this._callbacks || !this._callbacks[event]) return this;

    const index = this._callbacks[event].indexOf(callback);
    if (index !== -1) this._callbacks[event].splice(index, 1);
    return this;
  }

  /**
   * イベントに紐づく各コールバック関数を実行する
   * @param {string} event イベント名
   * @param {...*} args コールバック関数に渡す引数（可変長）
   * @returns {Event} <code>this</code>
   */
  trigger(event, ...args) {
    if (!this._callbacks || !this._callbacks[event]) return this;

    this._callbacks[event].forEach((callback) => {
      callback.apply(null, args);
    });
    return this;
  }
}

module.exports = Event;
