require('./css/app.css');
require('./css/normalize.css');
require('./css/sample.css');

const Sample = require('./js/controller/Sample');

const sample = new Sample();
sample.init();
