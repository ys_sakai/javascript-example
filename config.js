/**
 * 設定
 * @type {Object}
 */
const config = {
  /**
   * DB設定
   * @type {Object}
   */
  database: {
    host: 'localhost',
    port: 5432,
    database: 'sample',
    user: 'postgres',
    password: 'password'
  }
};

module.exports = config;
